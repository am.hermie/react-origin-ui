import axios from "axios";
import axiosRetry from 'axios-retry';
import { BASE_API, BASE_PATH_API } from './apiConfig'

const getConfig = token => {
  let config = {
    baseURL: BASE_API + BASE_PATH_API,
    headers: {}
  };

  if (token) {
    config.headers.Authorization = 'Bearer ' + token;
  }

  return config;
};

const axiosSuccess = result => {
  loading(false);
  return result.data;
};

const axiosError = (error) => {
  loading(false);

  console.error(error.message);
  console.error(error.response);

  // if (error.response && error.response.status === 401) {
  //   // unauthorize
  //   logout();
  // }

  return error;
};

export const loading = (isShow) => {

}

const axiosService = (type, url, params, extraParams) => {
  let token = null

  if (!extraParams || !extraParams.noToken) {
  }
  //condition token here 
  let config = getConfig(token);
  
  axiosRetry(axios, {
    retries: 3,
    retryDelay: count => {
      return count * 300;
    },
    retryCondition: error => {
      return (error.message || "").toLowerCase() === 'network error';
    }
  });

  //loading(true);
  switch (type) {
    case "get":
      return axios
        .get(url, config)
        .then(axiosSuccess)
        .catch(axiosError);

    case "post":
      return axios
        .post(url, params, config)
        .then(axiosSuccess)
        .catch(axiosError);

    case "put":
      return axios
        .put(url, params, config)
        .then(axiosSuccess)
        .catch(axiosError);

    case "delete":
      return axios
        .delete(url, params, config)
        .then(axiosSuccess)
        .catch(axiosError);

    case "postFormData":
      let formData = new FormData();
      Object.keys(params).map(key => {
        let value = params[key];
        if (typeof value === "object" && value.length > 0) {
          return value.map(subValue => {
            return formData.append(key, subValue);
          });
        } else return formData.append(key, params[key]);
      });
      config.headers["Content-Type"] = "multipart/form-data";
      return axios({
        method: "post",
        url: url,
        data: formData,
        //...config
      })
        .then(axiosSuccess)
        .catch(axiosError);

    default:
      return false;
  }
};

export default {
  get: (url, params, extraParams) => axiosService("get", url, params, extraParams),
  post: (url, params, extraParams) => axiosService("post", url, params, extraParams),
  put: (url, params, extraParams) => axiosService("put", url, params, extraParams),
  delete: (url, params, extraParams) => axiosService("delete", url, params, extraParams),
  postFormData: (url, params, extraParams) => axiosService("postFormData", url, params, extraParams),
};
