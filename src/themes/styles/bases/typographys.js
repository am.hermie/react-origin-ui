import {
  css
} from 'styled-components'
import {
  default as VARIABLES
} from './variables' // Use relative path for React Styleguidist

// //////////////////////////////////////////////////////////////////////////////////////////////////
// ==================================================================================================
//
// Typographys:
//
// Using: ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_MN};
//
// * First regular (Prompt Regular)
//   - FIRST_REGULAR_MN  (12px)
//   - FIRST_REGULAR_TN  (14px)
//   - FIRST_REGULAR_XXS (16px)
//   - FIRST_REGULAR_XS  (18px)
//   - FIRST_REGULAR_SM  (20px)
//   - FIRST_REGULAR_MD  (24px)
//   - FIRST_REGULAR_LG  (48px)
//
// * First medium (Prompt Medium)
//   - FIRST_MEDIUM_MN  (12px)
//   - FIRST_MEDIUM_TN  (14px)
//   - FIRST_MEDIUM_XXS (16px)
//   - FIRST_MEDIUM_XS  (18px)
//   - FIRST_MEDIUM_SM  (20px)
//   - FIRST_MEDIUM_MD  (24px)
//   - FIRST_MEDIUM_LG  (48px)
//
// * First bold (Prompt Bold)
//   - FIRST_BOLD_MN  (12px)
//   - FIRST_BOLD_TN  (14px)
//   - FIRST_BOLD_XXS (16px)
//   - FIRST_BOLD_XS  (18px)
//   - FIRST_BOLD_SM  (20px)
//   - FIRST_BOLD_MD  (24px)
//   - FIRST_BOLD_LG  (48px)
//
// Notice: Color, Hover/Focus can set at Global or Scaffolding
//
// ==================================================================================================
// //////////////////////////////////////////////////////////////////////////////////////////////////

// Text default style
// ============================================================
const FONT_PROPERTY_DEFAULT = css`
  font-weight: normal;
  vertical-align: middle;
  text-transform: none;
`

export default {
  FONT_STYLES: {
    // First regular
    // ============================================================

    // Mini
    // -------------------------------
    FIRST_REGULAR_MN: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.MN};
      line-height: ${VARIABLES.LINE_HEIGHTS.MN};
    `,

    // Tiny
    // -------------------------------
    FIRST_REGULAR_TN: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.TN};
      line-height: ${VARIABLES.LINE_HEIGHTS.TN};
    `,

    // Extra extra small
    // -------------------------------
    FIRST_REGULAR_XXS: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.XXS};
      line-height: ${VARIABLES.LINE_HEIGHTS.XXS};
    `,

    // Extra small
    // -------------------------------
    FIRST_REGULAR_XS: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.XS};
      line-height: ${VARIABLES.LINE_HEIGHTS.XS};
    `,

    // Small
    // -------------------------------
    FIRST_REGULAR_SM: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.SM};
      line-height: ${VARIABLES.LINE_HEIGHTS.SM};
    `,

    // Medium
    // -------------------------------
    FIRST_REGULAR_MD: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.MD};
      line-height: ${VARIABLES.LINE_HEIGHTS.MD};
    `,

    // Large
    // -------------------------------
    FIRST_REGULAR_LG: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_REGULAR};
      font-size: ${VARIABLES.FONT_SIZES.LG};
      line-height: ${VARIABLES.LINE_HEIGHTS.LG};
    `,

    // First medium
    // ============================================================

    // Mini
    // -------------------------------
    FIRST_MEDIUM_MN: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.MN};
      line-height: ${VARIABLES.LINE_HEIGHTS.MN};
    `,

    // Tiny
    // -------------------------------
    FIRST_MEDIUM_TN: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.TN};
      line-height: ${VARIABLES.LINE_HEIGHTS.TN};
    `,

    // Extra extra small
    // -------------------------------
    FIRST_MEDIUM_XXS: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.XXS};
      line-height: ${VARIABLES.LINE_HEIGHTS.XXS};
    `,

    // Extra small
    // -------------------------------
    FIRST_MEDIUM_XS: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.XS};
      line-height: ${VARIABLES.LINE_HEIGHTS.XS};
    `,

    // Small
    // -------------------------------
    FIRST_MEDIUM_SM: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.SM};
      line-height: ${VARIABLES.LINE_HEIGHTS.SM};
    `,

    // Medium
    // -------------------------------
    FIRST_MEDIUM_MD: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.MD};
      line-height: ${VARIABLES.LINE_HEIGHTS.MD};
    `,

    // Large
    // -------------------------------
    FIRST_MEDIUM_LG: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_MEDIUM};
      font-size: ${VARIABLES.FONT_SIZES.LG};
      line-height: ${VARIABLES.LINE_HEIGHTS.LG};
    `,

    // First bold
    // ============================================================

    // Mini
    // -------------------------------
    FIRST_BOLD_MN: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.MN};
      line-height: ${VARIABLES.LINE_HEIGHTS.MN};
    `,

    // Tiny
    // -------------------------------
    FIRST_BOLD_TN: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.TN};
      line-height: ${VARIABLES.LINE_HEIGHTS.TN};
    `,

    // Extra extra small
    // -------------------------------
    FIRST_BOLD_XXS: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.XXS};
      line-height: ${VARIABLES.LINE_HEIGHTS.XXS};
    `,

    // Extra small
    // -------------------------------
    FIRST_BOLD_XS: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.XS};
      line-height: ${VARIABLES.LINE_HEIGHTS.XS};
    `,

    // Small
    // -------------------------------
    FIRST_BOLD_SM: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.SM};
      line-height: ${VARIABLES.LINE_HEIGHTS.SM};
    `,

    // Medium
    // -------------------------------
    FIRST_BOLD_MD: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.MD};
      line-height: ${VARIABLES.LINE_HEIGHTS.MD};
    `,

    // Large
    // -------------------------------
    FIRST_BOLD_LG: css`
      ${FONT_PROPERTY_DEFAULT}
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      font-size: ${VARIABLES.FONT_SIZES.LG};
      line-height: ${VARIABLES.LINE_HEIGHTS.LG};
    `
  }
}
