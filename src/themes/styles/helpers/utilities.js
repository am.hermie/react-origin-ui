import {
  default as VARIABLES
} from '../bases/variables' // Use relative path for React Styleguidist

// //////////////////////////////////////////////////////////////////////////////////////////////////
// ==================================================================================================
//
// Utilities
//
// * Font sizes / Border width / Border radius
//   > mn, tn, xxs, xs, sm, md, lg, xl, bg, hg, ms
//
// * Text / Background / Border colors
//   > black, white, red-1, green-1, blue-1
//   > text-head, text-sub-head, text-detail, text-link, call-to-action
//   > validation-error, validation-success
//
// * Flex directions
//   > row, row-reverse, column, column-reverse
//
// * Flex wrap
//   > wrap, nowrap
//
// * Flex aligns
//   > left, right, center, spacebetween, top, bottom, middle, spacebetween-vertical
//
// * Flex vertical aligns
//   > top, bottom, middle
//
// * Positions
//   > top-left, top-right, bottom-left, bottom-right, over-top, over-bottom
//
// * Z indexs
//   > 1, 2, 3, 4, 5, 6, 7, 8, 9, 10
//
// * Spacings
//   > bottom-mn, bottom-tn, bottom-xxs, bottom-xs, bottom-sm, bottom-md, bottom-lg, bottom-xlg, bottom-bg, bottom-hg, bottom-ms
//
// Using:
// ${UTILITIES.TEXT_COLORS()};
//
// ==================================================================================================
// //////////////////////////////////////////////////////////////////////////////////////////////////

export default {
  // Font sizes
  // ==================================================================================================
  FONT_SIZES: () => {
    return `
      &.is-fontsize-mn {
        font-size: ${VARIABLES.FONT_SIZES.MN};
        line-height: ${VARIABLES.LINE_HEIGHTS.MN};
      }

      &.is-fontsize-tn {
        font-size: ${VARIABLES.FONT_SIZES.TN};
        line-height: ${VARIABLES.LINE_HEIGHTS.TN};
      }

      &.is-fontsize-xxs {
        font-size: ${VARIABLES.FONT_SIZES.XXS};
        line-height: ${VARIABLES.LINE_HEIGHTS.XXS};
      }

      &.is-fontsize-xs {
        font-size: ${VARIABLES.FONT_SIZES.XS};
        line-height: ${VARIABLES.LINE_HEIGHTS.XS};
      }

      &.is-fontsize-sm {
        font-size: ${VARIABLES.FONT_SIZES.SM};
        line-height: ${VARIABLES.LINE_HEIGHTS.SM};
      }

      &.is-fontsize-md {
        font-size: ${VARIABLES.FONT_SIZES.MD};
        line-height: ${VARIABLES.LINE_HEIGHTS.MD};
      }

      &.is-fontsize-lg {
        font-size: ${VARIABLES.FONT_SIZES.LG};
        line-height: ${VARIABLES.LINE_HEIGHTS.LG};
      }

      &.is-fontsize-xl {
        font-size: ${VARIABLES.FONT_SIZES.XL};
        line-height: ${VARIABLES.LINE_HEIGHTS.XL};
      }

      &.is-fontsize-bg {
        font-size: ${VARIABLES.FONT_SIZES.BG};
        line-height: ${VARIABLES.LINE_HEIGHTS.BG};
      }

      &.is-fontsize-hg {
        font-size: ${VARIABLES.FONT_SIZES.HG};
        line-height: ${VARIABLES.LINE_HEIGHTS.HG};
      }

      &.is-fontsize-ms {
        font-size: ${VARIABLES.FONT_SIZES.MS};
        line-height: ${VARIABLES.LINE_HEIGHTS.MS};
      }
    `
  },

  // Text colors
  // ==================================================================================================
  TEXT_COLORS: () => {
    return `
      &.is-textcolor-black {
        color: ${VARIABLES.COLORS.BLACK};
      }

      &.is-textcolor-white {
        color: ${VARIABLES.COLORS.WHITE};
      }

      &.is-textcolor-red-1 {
        color: ${VARIABLES.COLORS.RED_1};
      }

      &.is-textcolor-green-1 {
        color: ${VARIABLES.COLORS.GREEN_1};
      }

      &.is-textcolor-blue-1 {
        color: ${VARIABLES.COLORS.BLUE_1};
      }

      &.is-textcolor-text-head {
        color: ${VARIABLES.COLORS.TEXT_HEAD};
      }

      &.is-textcolor-text-sub-head {
        color: ${VARIABLES.COLORS.TEXT_SUB_HEAD};
      }

      &.is-textcolor-text-detail {
        color: ${VARIABLES.COLORS.TEXT_DETAIL};
      }

      &.is-textcolor-text-link {
        color: ${VARIABLES.COLORS.TEXT_LINK};
      }

      &.is-textcolor-call-to-action {
        color: ${VARIABLES.COLORS.CALL_TO_ACTION};
      }

      &.is-textcolor-validation-error {
        color: ${VARIABLES.COLORS.SYSTEM_ERROR};
      }

      &.is-textcolor-validation-success {
        color: ${VARIABLES.COLORS.SYSTEM_SUCCESS};
      }
    `
  },

  // Background colors
  // ==================================================================================================
  BACKGROUND_COLORS: () => {
    return `
      &.is-bgcolor-black {
        background-color: ${VARIABLES.COLORS.BLACK};
      }

      &.is-bgcolor-white {
        background-color: ${VARIABLES.COLORS.WHITE};
      }

      &.is-bgcolor-red-1 {
        background-color: ${VARIABLES.COLORS.RED_1};
      }

      &.is-bgcolor-green-1 {
        background-color: ${VARIABLES.COLORS.GREEN_1};
      }

      &.is-bgcolor-blue-1 {
        background-color: ${VARIABLES.COLORS.BLUE_1};
      }

      &.is-bgcolor-text-head {
        background-color: ${VARIABLES.COLORS.TEXT_HEAD};
      }

      &.is-bgcolor-text-sub-head {
        background-color: ${VARIABLES.COLORS.TEXT_SUB_HEAD};
      }

      &.is-bgcolor-text-detail {
        background-color: ${VARIABLES.COLORS.TEXT_DETAIL};
      }

      &.is-bgcolor-text-link {
        background-color: ${VARIABLES.COLORS.TEXT_LINK};
      }

      &.is-bgcolor-call-to-action {
        background-color: ${VARIABLES.COLORS.CALL_TO_ACTION};
      }

      &.is-bgcolor-validation-error {
        background-color: ${VARIABLES.COLORS.SYSTEM_ERROR};
      }

      &.is-bgcolor-validation-success {
        background-color: ${VARIABLES.COLORS.SYSTEM_SUCCESS};
      }
    `
  },

  // Border colors
  // ==================================================================================================
  BORDER_COLORS: () => {
    return `
      &.is-bordercolor-black {
        color: ${VARIABLES.COLORS.BLACK};
      }

      &.is-bordercolor-white {
        color: ${VARIABLES.COLORS.WHITE};
      }

      &.is-bordercolor-red-1 {
        color: ${VARIABLES.COLORS.RED_1};
      }

      &.is-bordercolor-green-1 {
        color: ${VARIABLES.COLORS.GREEN_1};
      }

      &.is-bordercolor-blue-1 {
        color: ${VARIABLES.COLORS.BLUE_1};
      }

      &.is-bordercolor-text-head {
        color: ${VARIABLES.COLORS.TEXT_HEAD};
      }

      &.is-bordercolor-text-sub-head {
        color: ${VARIABLES.COLORS.TEXT_SUB_HEAD};
      }

      &.is-bordercolor-text-detail {
        color: ${VARIABLES.COLORS.TEXT_DETAIL};
      }

      &.is-bordercolor-text-link {
        color: ${VARIABLES.COLORS.TEXT_LINK};
      }

      &.is-bordercolor-call-to-action {
        color: ${VARIABLES.COLORS.CALL_TO_ACTION};
      }

      &.is-bordercolor-validation-error {
        color: ${VARIABLES.COLORS.SYSTEM_ERROR};
      }

      &.is-bordercolor-validation-success {
        color: ${VARIABLES.COLORS.SYSTEM_SUCCESS};
      }
    `
  },

  // Border width
  // ==================================================================================================
  BORDER_WIDTHS: () => {
    return `
      &.is-borderwidth-mn {
        border: ${VARIABLES.BORDER_WIDTHS.MN};
      }

      &.is-borderwidth-tn {
        border: ${VARIABLES.BORDER_WIDTHS.TN};
      }

      &.is-borderwidth-xxs {
        border: ${VARIABLES.BORDER_WIDTHS.XXS};
      }

      &.is-borderwidth-xs {
        border: ${VARIABLES.BORDER_WIDTHS.XS};
      }

      &.is-borderwidth-sm {
        border: ${VARIABLES.BORDER_WIDTHS.SM};
      }

      &.is-borderwidth-md {
        border: ${VARIABLES.BORDER_WIDTHS.MD};
      }

      &.is-borderwidth-lg {
        border: ${VARIABLES.BORDER_WIDTHS.LG};
      }

      &.is-borderwidth-xl {
        border: ${VARIABLES.BORDER_WIDTHS.XL};
      }

      &.is-borderwidth-bg {
        border: ${VARIABLES.BORDER_WIDTHS.BG};
      }

      &.is-borderwidth-hg {
        border: ${VARIABLES.BORDER_WIDTHS.HG};
      }

      &.is-borderwidth-ms {
        border: ${VARIABLES.BORDER_WIDTHS.MS};
      }
    `
  },

  // Border radius
  // ==================================================================================================
  BORDER_RADIUSES: () => {
    return `
      &.is-borderradius-mn {
        border-radius: ${VARIABLES.BORDER_RADIUSES.MN};
      }

      &.is-borderradius-tn {
        border-radius: ${VARIABLES.BORDER_RADIUSES.TN};
      }

      &.is-borderradius-xxs {
        border-radius: ${VARIABLES.BORDER_RADIUSES.XXS};
      }

      &.is-borderradius-xs {
        border-radius: ${VARIABLES.BORDER_RADIUSES.XS};
      }

      &.is-borderradius-sm {
        border-radius: ${VARIABLES.BORDER_RADIUSES.SM};
      }

      &.is-borderradius-md {
        border-radius: ${VARIABLES.BORDER_RADIUSES.MD};
      }

      &.is-borderradius-lg {
        border-radius: ${VARIABLES.BORDER_RADIUSES.LG};
      }

      &.is-borderradius-xl {
        border-radius: ${VARIABLES.BORDER_RADIUSES.XL};
      }

      &.is-borderradius-bg {
        border-radius: ${VARIABLES.BORDER_RADIUSES.BG};
      }

      &.is-borderradius-hg {
        border-radius: ${VARIABLES.BORDER_RADIUSES.HG};
      }

      &.is-borderradius-ms {
        border-radius: ${VARIABLES.BORDER_RADIUSES.MS};
      }
    `
  },

  // Flex directions
  // ==================================================================================================
  FLEX_DIRECTIONS: () => {
    return `
      &.is-flexdirection-row {
        flex-direction: row;
      }

      &.is-flexdirection-row-reverse {
        flex-direction: row;
      }

      &.is-flexdirection-column {
        flex-direction: column;
      }

      &.is-flexdirection-column-reverse {
        flex-direction: column;
      }
    `
  },

  // Flex wraps
  // ==================================================================================================
  FLEX_WRAPS: () => {
    return `
      &.is-flexwrap-wrap {
        flex-wrap: wrap;
      }

      &.is-flexwrap-nowrap {
        flex-wrap: nowrap;
      }
    `
  },

  // Flex aligns
  // ==================================================================================================
  FLEX_ALIGNS: () => {
    return `
      &.is-flexalign-left {
        justify-content: flex-start;
      }
      &.is-flexalign-right {
        justify-content: flex-end;
      }
      &.is-flexalign-center {
        justify-content: center;
      }
      &.is-flexalign-spacebetween {
        justify-content: space-between;
      }

      &.is-flexalign-top {
        flex-direction: column;
        justify-content: flex-start;
      }
      &.is-flexalign-bottom {
        flex-direction: column;
        justify-content: flex-end;
      }
      &.is-flexalign-middle {
        flex-direction: column;
        justify-content: center;
      }
      &.is-flexalign-spacebetween-vertical {
        flex-direction: column;
        justify-content: space-between;
      }
    `
  },

  // Flex vertical aligns
  // ==================================================================================================
  FLEX_ALIGN_ITEMS: () => {
    return `
      &.is-flexalign-vertical-top {
        align-items: flex-start;
      }

      &.is-flexalign-vertical-bottom {
        align-items: flex-end;
      }

      &.is-flexalign-vertical-middle {
        align-items: center;
      }
    `
  },

  // Absolute position
  // ==================================================================================================
  POSITIONS: () => {
    return `
      &.is-position-top-left {
        top: 0;
        left: 0;
      }

      &.is-position-top-right {
        top: 0;
        right: 0;
      }

      &.is-position-bottom-left {
        bottom: 0;
        left: 0;
      }

      &.is-position-bottom-right {
        bottom: 0;
        right: 0;
      }

      &.is-position-over-top {
        top: 100%;
        left: 0
      }

      &.is-position-over-bottom {
        bottom: 100%;
        left: 0
      }
    `
  },

  // Z indexs
  // ==================================================================================================
  Z_INDEX_LEVELS: () => {
    return `
      &.is-zindex-level-1 {
        z-index: 1;
      }

      &.is-zindex-level-2 {
        z-index: 9;
      }

      &.is-zindex-level-3 {
        z-index: 10;
      }

      &.is-zindex-level-4 {
        z-index: 11;
      }

      &.is-zindex-level-5 {
        z-index: 99;
      }

      &.is-zindex-level-6 {
        z-index: 100;
      }

      &.is-zindex-level-7 {
        z-index: 101;
      }

      &.is-zindex-level-8 {
        z-index: 999;
      }

      &.is-zindex-level-9 {
        z-index: 1000;
      }

      &.is-zindex-level-10 {
        z-index: 1001;
      }
    `
  },

  // Spacings
  // ==================================================================================================
  SPACINGS: () => {
    return `
      &.is-spacing-bottom-mn {
        margin-bottom: 5px;
      }

      &.is-spacing-bottom-tn {
        margin-bottom: 10px;
      }

      &.is-spacing-bottom-xxs {
        margin-bottom: 15px;
      }

      &.is-spacing-bottom-xs {
        margin-bottom: 20px;
      }

      &.is-spacing-bottom-sm {
        margin-bottom: 25px;
      }

      &.is-spacing-bottom-md {
        margin-bottom: 30px;
      }

      &.is-spacing-bottom-lg {
        margin-bottom: 35px;
      }

      &.is-spacing-bottom-xlg {
        margin-bottom: 40px;
      }

      &.is-spacing-bottom-bg {
        margin-bottom: 45px;
      }

      &.is-spacing-bottom-hg {
        margin-bottom: 50px;
      }

      &.is-spacing-bottom-ms {
        margin-bottom: 55px;
      }
    `
  }
}
