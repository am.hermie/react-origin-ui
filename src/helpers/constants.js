export const ROUTE_PATH = {
  HOME: {
    TEXT: 'Home',
    LINK: '/'
  },

  DETAIL: {
    TEXT: 'Detail',
    LINK: '/detail'
  },

  LAB: {
    TEXT: 'Lab',
    LINK: '/lab',
    EXAMPLES: {
      FIELD: {
        TEXT: 'Field example',
        LINK: '/lab/examples/field'
      },
      MAP_DATA: {
        TEXT: 'Map data example',
        LINK: '/lab/examples/mapdata'
      },
      SORTABLE: {
        TEXT: 'React Anything Sortable example',
        LINK: '/lab/examples/sortable'
      },
      TEXT_EDITOR: {
        TEXT: 'Text editor example',
        LINK: '/lab/examples/texteditor'
      },
      DATETIME_PICKER: {
        TEXT: 'Datetime picker example',
        LINK: '/lab/examples/datetimepicker'
      },
      PAGINATION: {
        TEXT: 'Pagination example',
        LINK: '/lab/examples/pagination'
      },
      ACCORDION: {
        TEXT: 'Accordion example',
        LINK: '/lab/examples/accordion'
      }
    }
  }
}

export const REACT_QUILL_OPTION = {
  TOOLBAR: [
    [
      {
        'header': [
          1,
          2,
          3,
          4,
          5,
          6,
          false
        ]
      }
    ],
    [
      'bold',
      'italic',
      'underline',
      'strike',
      'blockquote'
    ],
    [
      { 'script': 'sub' },
      { 'script': 'super' }
    ],
    [
      {
        'color': [
          '#FF0000',
          '#00FF00',
          '#0000FF'
        ]
      },
      {
        'background': [
          '#FF0000',
          '#00FF00',
          '#0000FF'
        ]
      }
    ],
    [
      { 'align': [] }
    ],
    [
      { 'list': 'ordered' },
      { 'list': 'bullet' },
    ],
    [
      { 'indent': '-1' },
      { 'indent': '+1' }
    ],
    [
      'link',
      'image',
      'video'
    ],
    [
      'clean'
    ]
  ],

  FORMATS: [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'color', 'background',
    'script',
    'align',
    'list', 'bullet',
    'indent',
    'link', 'image', 'video'
  ]
}
