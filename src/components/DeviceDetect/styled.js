import styled from 'styled-components'
// import {
//   default as VARIABLES
// } from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const DeviceDetectWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  /* Modifiers
  ------------------------------- */
  /* Safari browser only not iOS webview */
  /* Use with react-window-decorators for toggle class when
     1. scroll position '0' use 'is-ui-fix-bounce'
     2. scroll position '> 0' use 'is-ui-fix-bounce-out'
  */
  &.is-ios {
    transition-property: transform;
    transition-duration: 0.2s;
    /* easeOutSine */
    /* Sample - https://easings.net/ */
    /* Collection - https://gist.github.com/snorpey/6804b3b5adf204a6c9f5 */
    transition-timing-function: cubic-bezier(0.390, 0.575, 0.565, 1.000);

    &.is-ui-fix-bounce {
      transform: translateY(0);
      position: fixed;
      width: 100%;
      height: 100%;
      overflow: hidden;
    }

    &.is-ui-fix-bounce-out {
      transition-duration: 0s;
      transform: translateY(125px);
    }
  }

  /* Media queries
  ------------------------------- */
`
