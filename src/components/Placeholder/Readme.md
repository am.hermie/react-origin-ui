Image:

```jsx
<Placeholder>
  <Placeholder.Content isImage />
</Placeholder>
```

Barcode:

```jsx
<Placeholder>
  <Placeholder.Content isBarcode />
</Placeholder>
```

List:

```jsx
<Placeholder>
  <Placeholder.Content isList />
</Placeholder>
```

Size:

```jsx
<Placeholder size='mobile-lg'>
  <Placeholder.Content isImage />
</Placeholder>
<br />
<Placeholder size='mobile-lg'>
  <Placeholder.Content isBarcode />
</Placeholder>
<br />
<Placeholder size='mobile-lg'>
  <Placeholder.Content isList />
</Placeholder>
```
