import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import ContentLoader from 'react-content-loader'
import {
  PlaceholderWrapper
} from './styled'

class PlaceholderContent extends React.PureComponent {
  static defaultProps = {
    speed: 2,
    primaryColor: '#f3f3f3',
    secondaryColor: '#ecebeb'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Light color speed animation in svg
    */
    speed: PropTypes.number,

    /**
    * Background color in svg
    */
    primaryColor: PropTypes.string,

    /**
    * Light color over background co in svg
    */
    secondaryColor: PropTypes.string,

    /**
    * Display image placeholder
    */
    isImage: PropTypes.bool,

    /**
    * Display barcode placeholder
    */
    isBarcode: PropTypes.bool,

    /**
    * Display list placeholder
    */
    isList: PropTypes.bool
  }

  render () {
    const {
      className,
      children,
      ui,
      speed,
      primaryColor,
      secondaryColor,
      isBarcode,
      isImage,
      isList
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'placeholder-content',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div className={classes}>
        {
          isImage &&
            <ContentLoader
              // ContentLoader 'width' and 'height' prop is default value from generate SVG tool.
              // If you want to control Placeholder size, please ignore default value and control by container width (Placeholder 'size' prop)
              width={480}
              height={270}
              speed={speed}
              primaryColor={primaryColor}
              secondaryColor={secondaryColor}
            >
              <rect x='0' y='0' rx='0' ry='0' width='480' height='270' />
            </ContentLoader>
        }

        {
          isBarcode &&
            <ContentLoader
              // ContentLoader 'width' and 'height' prop is default value from generate SVG tool.
              // If you want to control Placeholder size, please ignore default value and control by container width (Placeholder 'size' prop)
              width={310}
              height={90}
              speed={speed}
              primaryColor={primaryColor}
              secondaryColor={secondaryColor}
            >
              <rect x='1.84' y='0' rx='0' ry='0' width='19.8' height='90' />
              <rect x='26.84' y='0' rx='0' ry='0' width='8' height='90' />
              <rect x='41.84' y='0' rx='0' ry='0' width='50' height='90' />
              <rect x='96.84' y='0' rx='0' ry='0' width='12' height='90' />
              <rect x='110.84' y='0' rx='0' ry='0' width='8' height='90' />
              <rect x='120.84' y='0' rx='0' ry='0' width='4' height='90' />
              <rect x='125.84' y='0' rx='0' ry='0' width='0' height='90' />
              <rect x='130.84' y='0' rx='0' ry='0' width='24' height='90' />
              <rect x='159.84' y='0' rx='0' ry='0' width='7' height='90' />
              <rect x='169.84' y='0' rx='0' ry='0' width='17' height='90' />
              <rect x='187.84' y='0' rx='0' ry='0' width='6' height='90' />
              <rect x='196.84' y='0' rx='0' ry='0' width='2' height='90' />
              <rect x='201.84' y='0' rx='0' ry='0' width='32' height='90' />
              <rect x='237.84' y='0' rx='0' ry='0' width='7' height='90' />
              <rect x='247.84' y='0' rx='0' ry='0' width='28' height='90' />
              <rect x='277.84' y='0' rx='0' ry='0' width='7' height='90' />
              <rect x='285.84' y='0' rx='0' ry='0' width='4' height='90' />
              <rect x='291.84' y='0' rx='0' ry='0' width='30' height='90' />
            </ContentLoader>
        }

        {
          isList &&
            <ContentLoader
              // ContentLoader 'width' and 'height' prop is default value from generate SVG tool.
              // If you want to control Placeholder size, please ignore default value and control by container width (Placeholder 'size' prop)
              width={320}
              height={100}
              speed={speed}
              primaryColor={primaryColor}
              secondaryColor={secondaryColor}
            >
              <circle cx='10' cy='11' r='8' />
              <rect x='25' y='6' rx='5' ry='5' width='290' height='10' />
              <circle cx='10' cy='41' r='8' />
              <rect x='25' y='36' rx='5' ry='5' width='290' height='10' />
              <circle cx='10' cy='71' r='8' />
              <rect x='25' y='66' rx='5' ry='5' width='290' height='10' />
            </ContentLoader>
        }

        {children}
      </div>
    )
  }
}

/**
 * Placeholder description:
 * - placeholder svg for preload display like facebook
 * - https://github.com/danilowoz/react-content-loader
 * - Generate SVG > http://danilowoz.com/create-content-loader
 */

export class Placeholder extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Size
    */
    size: PropTypes.oneOf([
      'mobile-lg'
    ])
  }

  static Content = PlaceholderContent

  render () {
    const {
      className,
      children,
      ui,
      size
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const sizeClasses = ClassNames(size)
    const classes = ClassNames(
      'placeholder',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-size-${sizeClasses}`]: sizeClasses },
      className
    )

    return (
      <PlaceholderWrapper
        className={classes}
      >
        {children}
      </PlaceholderWrapper>
    )
  }
}
