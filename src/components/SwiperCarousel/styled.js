import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const SwiperCarouselWrapper = styled.div`
  /* Parent
  ------------------------------- */
  width: 100%;

  /* Childrens
  ------------------------------- */
  .swiper-carousel-item {
    display: block;
  }

  /* Overwrite default swiper classes */
  .swiper-container {
    &.swiper-container-horizontal {
      /* Pagination */
      > .swiper-pagination-bullets {
        bottom: 12px;

        .swiper-pagination-bullet {
          transition: ${VARIABLES.TRANSITIONS.DEFAULT};
          opacity: 1;
          width: 8px;
          height: 8px;
          background-color: rgba(255, 255, 255, 0.5);
          border-radius: 50%;

          &.swiper-pagination-bullet-active {
            width: 25px;
            background-color: ${VARIABLES.COLORS.WHITE};
            border-radius: 8px;
          }
        }
      }
    }
  }

  /* Modifiers
  ------------------------------- */
  &.is-ui-card {
    .swiper-carousel-item {
      width: 162px;
      margin-left: 7px;
      margin-bottom: 5px;

      &:first-child {
        margin-left: 15px;
      }

      &:last-child {
        margin-right: 15px;
      }

      @media (max-width: ${VARIABLES.BREAKPOINTS.MOBILE_XS_MAX}) {
        width: 132px;
      }

      @media (min-width: ${VARIABLES.BREAKPOINTS.MOBILE_SM}) and (max-width: ${VARIABLES.BREAKPOINTS.MOBILE_SM_MAX}) {
        width: 152px;
      }
    }
  }

  /* Media queries
  ------------------------------- */
`
