import styled  from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const BarcodePanelWrapper = styled.div`
  /* Parent
  ------------------------------- */
  width: 100%;

  /* Childrens
  ------------------------------- */
  .barcode-panel-container {
    position: relative;
    padding: 25px;
  }

  .barcode-panel-generate {
    > svg {
      width: 100%;

      > rect,
      text {
        display: none;
      }
    }
  }

  .barcode-panel-code {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    padding-top: 3px;
    color: ${VARIABLES.COLORS.TEXT_HEAD};
    letter-spacing: 2px;
    text-align: center;
  }

  .barcode-panel-note {
    padding-top: 20px;
  }

  .barcode-panel-note-primary {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_BOLD_XS};
    color: ${VARIABLES.COLORS.TEXT_HEAD};
    text-align: center;
  }

  .barcode-panel-note-secondary {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_XXS};
    color: ${VARIABLES.COLORS.TEXT_SUB_HEAD};
    text-align: center;
  }

  /* Modifiers
  ------------------------------- */
  /* Media queries
  ------------------------------- */
`
