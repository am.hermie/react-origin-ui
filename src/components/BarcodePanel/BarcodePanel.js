import React from 'react'
import Barcode from 'react-barcode'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  BarcodePanelWrapper
} from './styled'

/**
 * BarcodePanel description:
 * - Generate barcode
 * - https://github.com/kciter/react-barcode/ (this library wrapped JsBarcode - https://github.com/lindell/JsBarcode)
 */

export class BarcodePanel extends React.PureComponent {
  static defaultProps = {
    format: 'CODE128',
    value: '1234567890123',
    height: 90,
    margin: 0
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Format (https://github.com/lindell/JsBarcode/wiki#barcodes)
    */
    format: PropTypes.oneOf([
      'CODE128',
      'EAN13',
      'UPC',
      'EAN8',
      'EAN5',
      'EAN2',
      'CODE39',
      'ITF14',
      'MSI',
      'pharmacode',
      'codabar'
    ]),

    /**
    * Code
    */
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number
    ]),

    /**
    * Height
    */
    height: PropTypes.number,

    /**
    * Margin
    */
    margin: PropTypes.number,

    /**
    * Heading
    */
    heading: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Sub Heading
    */
    subHeading: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      format,
      value,
      height,
      margin,
      heading,
      subHeading
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'barcode-panel',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <BarcodePanelWrapper
        className={classes}
      >
        <div className='barcode-panel-container'>
          <div className='barcode-panel-generate'
            style={{
              height: height
            }}
          >
            <Barcode
              format={format}
              value={value} // UI > Default 13 number, Max 20 number
              height={height}
              margin={margin}
            />
          </div>
          <div className='barcode-panel-code'>
            {value}
          </div>
          {
            (heading || subHeading) &&
              <div className='barcode-panel-note'>
                <p className='barcode-panel-note-primary'>
                  {heading}
                </p>
                <p className='barcode-panel-note-secondary'>
                  {subHeading}
                </p>
              </div>
          }
        </div>
        {children}
      </BarcodePanelWrapper>
    )
  }
}
