import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
import {
  default as MIXINS
} from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-DateTimePicker.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const DateTimePickerWrapper = styled.div`
  /* Parent
  ------------------------------- */
  display: flex;

  /* Childrens
  ------------------------------- */
  .field-input-label {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_BOLD_TN};
    padding-top: 7px;
    margin-right: 10px;
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  .field-input-group {
    position: relative;
  }

  .field-input-message,
  .field-input-hint {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_MN};
    color: ${VARIABLES.COLORS.TEXT_DETAIL};
  }

  .field-input-message {
    display: none;
    margin-top: 5px;
  }

  .field-input-hint {
    padding-top: 8px;
    margin-left: 10px;
  }

  /* Modifiers
  ------------------------------- */
  /* UI */
  &.is-ui-disabled {
    /* React Date Picker */
    &.is-ui-react-datepicker {
      .react-datepicker__input-container {
        input {
          pointer-events: none;
          background-color: ${VARIABLES.COLORS.GRAY_1};
        }
      }

      .react-datepicker__close-icon {
        display: none;
      }
    }
  }

  /* UI - Validation */
  &.is-ui-validation-error {
    /* React Date Picker */
    &.is-ui-react-datepicker {
      .react-datepicker__input-container {
        input {
          border-color: ${VARIABLES.COLORS.SYSTEM_ERROR};
        }
      }

      .field-input-message {
        display: block;
        color: ${VARIABLES.COLORS.SYSTEM_ERROR};
      }
    }
  }

  /* UI - Module */
  /* React Date Picker */
  &.is-ui-react-datepicker {
    .react-datepicker__input-container {
      input {
        ${MIXINS.PLACEHOLDER({})};
        ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
        width: ${VARIABLES.FIELD.WIDTHS.XS};
        height: ${VARIABLES.FIELD.TYPES.TEXT.HEIGHT};
        padding: 5px 30px 5px 10px;
        background-color: ${VARIABLES.COLORS.WHITE};
        border: 1px solid ${VARIABLES.COLORS.TEXT_DETAIL};
        border-radius: ${VARIABLES.FIELD.BORDER_RADIUS};
        color: ${VARIABLES.COLORS.TEXT_HEAD};
      }
    }

    .react-datepicker__time-container {
      .react-datepicker__header {
        &.react-datepicker__header--time {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 59px;
        }
      }

      .react-datepicker__time {
        .react-datepicker__time-box {
          ul.react-datepicker__time-list {
            li.react-datepicker__time-list-item {
              height: auto;
            }

            li.react-datepicker__time-list-item--selected {
              background-color: ${VARIABLES.COLORS.TEXT_HEAD};
            }
          }
        }
      }
    }

    .react-datepicker__day--selected,
    .react-datepicker__day--in-selecting-range,
    .react-datepicker__day--in-range {
      background-color: ${VARIABLES.COLORS.TEXT_HEAD};

      &:hover {
        background-color: ${VARIABLES.COLORS.TEXT_HEAD};
      }
    }

    .react-datepicker__close-icon {
      top: 0;
      right: 0;
      width: 30px;
      height: 100%;

      &:after {
        position: static;
        margin: 0;
        background-color: transparent;
        width: auto;
        height: auto;
        font-size: ${VARIABLES.FONT_SIZES.XXS};
        color: ${VARIABLES.COLORS.TEXT_HEAD};
      }
    }
  }

  /* Mode */
  &.is-mode-react-datepicker-datetime {
    .react-datepicker {
      width: 313px
    }

    .react-datepicker__month-container {
      position: relative;
      width: 241px;

      &:after {
        content: ' ';
        display: block;
        position: absolute;
        z-index: ${VARIABLES.Z_INDEXS.LV_1};
        top: 0;
        right: 0;
        width: 1px;
        height: 100%;
        background-color: #aeaeae;
      }
    }

    .react-datepicker__time-container {
      position: absolute;
      z-index: ${VARIABLES.Z_INDEXS.LV_1};
      top: 0;
      right: 0;
      height: 100%;
      border-left-width: 0;

      .react-datepicker__time {
        height: calc(100% - 59px);

        .react-datepicker__time-box {
          overflow: hidden;
          height: 100%;

          ul.react-datepicker__time-list {
            height: 100% !important; /* Overwrite inline style from script */
          }
        }
      }
    }
  }

  /* Label width */
  &.is-label-width {
    &-sm {
      .field-input-label {
        width: ${VARIABLES.FIELD.LABEL_WIDTHS.SM};
      }
    }

    &-md {
      .field-input-label {
        width: ${VARIABLES.FIELD.LABEL_WIDTHS.MD};
      }
    }

    &-lg {
      .field-input-label {
        width: ${VARIABLES.FIELD.LABEL_WIDTHS.LG};
      }
    }
  }

  /* Data width */
  &.is-data-width {
    &-sm {
      .react-datepicker__input-container {
        input {
          width: ${VARIABLES.FIELD.WIDTHS.SM};
        }
      }
    }

    &-md {
      .react-datepicker__input-container {
        input {
          width: ${VARIABLES.FIELD.WIDTHS.MD};
        }
      }
    }

    &-lg {
      .react-datepicker__input-container {
        input {
          width: ${VARIABLES.FIELD.WIDTHS.LG};
        }
      }
    }
  }

  /* Display */
  &.is-display-message {
    .field-input-message {
      display: block;
    }
  }

  /* Media queries
  ------------------------------- */
`
