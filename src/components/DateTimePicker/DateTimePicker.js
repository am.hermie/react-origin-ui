import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  DateTimePickerWrapper
} from './styled'

/**
 * DateTimePicker description:
 * - Wrapper/Container for add parent class and then overwrite default module style
 * - react-datepicker, https://github.com/Hacker0x01/react-datepicker/
 */

export class DateTimePicker extends React.PureComponent {
  static defaultProps = {
    isReactDatePicker: true
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'disabled',
      'validation-error',
      'react-datepicker'
    ]),

    /**
    * Modifier name for customize default module style
    */
    mode: PropTypes.oneOf([
      // react-date-picker - fix DatePicker container width when use 'showTimeSelect'
      'react-datepicker-datetime'
    ]),

    /**
    * Label (field name at front of input)
    */
    label: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Label width
    */
    labelWidth: PropTypes.oneOf([
      'sm',
      'md',
      'lg'
    ]),

    /**
    * Data width (insert block)
    */
    dataWidth: PropTypes.oneOf([
      'sm',
      'md',
      'lg'
    ]),

    /**
    * Hint (description at back of input)
    */
    hint: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Message (description at bottom of input)
    */
    message: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Display text info instead field
    */
    textInfo: PropTypes.string,

    /**
    * Enable React Date Picker overwrite style
    */
    isReactDatePicker: PropTypes.bool,

    /**
    * Modifier name for show/hide element
    */
    display: PropTypes.oneOf([
      'message'
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      mode,
      labelWidth,
      dataWidth,
      label,
      hint,
      message,
      textInfo,
      isReactDatePicker,
      display
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const modeClasses = ClassNames(mode)
    const labelWidthClasses = ClassNames(labelWidth)
    const dataWidthClasses = ClassNames(dataWidth)
    const displayClasses = ClassNames(display)
    const classes = ClassNames(
      'datetime-picker',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-ui-react-datepicker`]: isReactDatePicker },
      { [`is-mode-${modeClasses}`]: modeClasses },
      { [`is-label-width-${labelWidthClasses}`]: labelWidthClasses },
      { [`is-data-width-${dataWidthClasses}`]: dataWidthClasses },
      { [`is-display-${displayClasses}`]: displayClasses },
      className
    )

    return (
      <DateTimePickerWrapper
        className={classes}
      >
        {
          label &&
            <label className='field-input-label'>
              {label}
            </label>
        }
        <div className='field-input-group'>
          {
            !textInfo &&
              <React.Fragment>
                {children}
              </React.Fragment>
          }

          {
            textInfo &&
              <div className='field-input-text-info'>
                {textInfo}
              </div>
          }
          {
            message &&
              <div className='field-input-message'>
                {message}
              </div>
          }
        </div>
        {
          hint &&
            <label className='field-input-hint'>
              {hint}
            </label>
        }
      </DateTimePickerWrapper>
    )
  }
}
