import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  TransitionContainerWrapper
} from './styled'

/**
 * TransitionContainer description:
 * - use to wrap content in page conainer for enable page transition
 * - Default: [TransitionContainer] Insert content... [/TransitionContainer]
 * - Motion: [TransitionContainer motion='overlap'] Insert content... [/TransitionContainer]
 */

export class TransitionContainer extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Motion type of page transition
    */
    motion: PropTypes.oneOf([
      'overlap'
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      motion
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const motionClasses = ClassNames(motion)
    const classes = ClassNames(
      'transition-container',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-motion-${motionClasses}`]: motionClasses },
      className
    )

    return (
      <TransitionContainerWrapper
        className={classes}
      >
        <div className='transition-scroll-container'>
          {children}
        </div>
      </TransitionContainerWrapper>
    )
  }
}
