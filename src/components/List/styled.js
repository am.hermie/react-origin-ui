import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const ListWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  .list-text {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    position: relative;
    margin-bottom: 10px;
    color: ${VARIABLES.COLORS.TEXT_DETAIL};

    &:last-child {
      margin-bottom: 0;
    }

    /* UI */
    &.is-ui-heading {
      font-family: ${VARIABLES.FONT_FAMILIES.FIRST_BOLD};
      color: ${VARIABLES.COLORS.TEXT_HEAD};
    }

    /* Bullet */
    &.is-bullet-number-one {
      padding-left: 20px;
    }

    &.is-bullet-number-two {
      padding-left: 25px;
    }

    &.is-bullet-icon-size-sm {
      padding-left: 25px;

      .list-bullet {
        top: 2px;
      }

      /* Sample class for use in styleguide */
      .list-icon {
        width: auto;
        height: 14px;
      }
    }
  }

  .list-bullet {
    position: absolute;
    z-index: ${VARIABLES.Z_INDEXS};
    top: 0;
    left: 0;
  }

  /* Modifiers
  ------------------------------- */
  /* Media queries
  ------------------------------- */
`
