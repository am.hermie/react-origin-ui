Default:

```jsx
<List>
  <List.Text ui='heading'>Heading</List.Text>
  <List.Text>item</List.Text>
  <List.Text>item</List.Text>
  <List.Text>item</List.Text>
</List>
```

Bullet:

```jsx
<List>
  <List.Text ui='heading'>Heading</List.Text>
  <List.Text bullet='number-one' bulletItem='1.'> item</List.Text>
  <List.Text bullet='number-one' bulletItem='2.'> item</List.Text>
  <List.Text bullet='number-one' bulletItem='3.'> item</List.Text>
</List>
<br />
<List>
  <List.Text ui='heading'>Heading</List.Text>
  <List.Text bullet='number-two' bulletItem='11.'> item</List.Text>
  <List.Text bullet='number-two' bulletItem='22.'> item</List.Text>
  <List.Text bullet='number-two' bulletItem='33.'> item</List.Text>
</List>
<br />
<List>
  <List.Text ui='heading'>Heading</List.Text>
  <List.Text bullet='icon-size-sm' bulletItem={<img className='list-icon' src={require('./images/sample-icon.svg')} />}> item</List.Text>
  <List.Text bullet='icon-size-sm' bulletItem={<img className='list-icon' src={require('./images/sample-icon.svg')} />}> item</List.Text>
  <List.Text bullet='icon-size-sm' bulletItem={<img className='list-icon' src={require('./images/sample-icon.svg')} />}> item</List.Text>
</List>
<div style={{ paddingTop: '15px' }}>Note: 'bulletItem' props can insert any node eg. string, number, html, component but you must create modifier class for control spacing between bullet and text</div>
```
