import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  BootstrapWrapper
} from './styled'

/**
 * Bootstrap description:
 * - Bootstrap v4.1.3 (https://github.com/twbs/bootstrap)
 * - Grids, Document (https://getbootstrap.com/docs/4.1/layout/grid), CSS (https://github.com/twbs/bootstrap/blob/v4-dev/dist/css/bootstrap-grid.css)
 */

export class Bootstrap extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  }

  render () {
    const {
      children
    } = this.props

    // props for css classes
    const classes = ClassNames(
      'bootstrap'
    )

    return (
      <BootstrapWrapper
        className={classes}
      >
        {children}
      </BootstrapWrapper>
    )
  }
}
