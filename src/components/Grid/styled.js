// Import from CSS because styled-components is not support SCSS variables and functions
import './css/grid.min.css'

// Global style
import {
  injectGlobal
} from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'

// Fix sortable-item style of react-sortable-hoc when drag sortable-item, see scenario
  //
  // 1. drag sortable-item
  // 2. react-sortable-hoc will create sortable-item html copy at body
  // 3. component style doesn't working because a child element are outside a parent class
  // 4. use a child element global style for control this issue
  //
// !!! Please ensure, a child element global style doesn't overwrtire any style !!!

injectGlobal`
  /* Childrens
  ------------------------------- */
  .grid-column {
    z-index: ${VARIABLES.Z_INDEXS.LV_1};
  }

  /* Modifiers
  ------------------------------- */
  /* Media queries
  ------------------------------- */
`