import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  SampleWrapper
} from './styled'
import './css/sample-vendor.css'

/**
 * Sample description:
 * - Component structure template for copy to create new component
 */

export class Sample extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'sample',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <SampleWrapper
        className={classes}
      >
        {children}
      </SampleWrapper>
    )
  }
}
