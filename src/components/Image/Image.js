import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  ImageWrapper
} from './styled'

/**
 * Image description:
 * - component for display banner, thumbnail, icon and more
 */

export class Image extends React.PureComponent {
  static defaultProps = {
    ratio: '16-9',
    alt: 'Image',
    altPlaceholder: 'Placeholder'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'curve-border'
    ]),

    /**
    * Control width
    */
    width: PropTypes.oneOf([
      'icon-mn',
      'icon-tn',
      'icon-xxs',
      'icon-xs',
      'icon-sm',
      'icon-md',
      'icon-lg',
      'icon-xl',
      'icon-bg',
      'icon-hg',
      'icon-ms'
    ]),

    /**
    * Control ratio
    */
    ratio: PropTypes.oneOf([
      '16-9',
      '4-3',
      '1-1'
    ]),

    /**
    * Url image for lazy load >>> 'data-src' (props from react-id-swiper) use instead src for enable lazyload
    */
    dataSrc: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string
    ]),

    /**
    * Url image (default)
    */
    src: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string
    ]),

    /**
    * Altenative text
    */
    alt: PropTypes.string,

    /**
    * Placeholder url image for lazy load >>> 'data-src' (props from react-id-swiper) use instead src for enable lazyload
    */
    dataSrcPlaceholder: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string
    ]),

    /**
    * Placeholder url image (default)
    */
    srcPlaceholder: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string
    ]),

    /**
    * Placeholder altenative text
    */
    altPlaceholder: PropTypes.string
  }

  render () {
    const {
      className,
      ui,
      children,
      width,
      ratio,
      dataSrc,
      src,
      alt,
      dataSrcPlaceholder,
      srcPlaceholder,
      altPlaceholder
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const ratioClasses = ClassNames(ratio)
    const widthClasses = ClassNames(width)
    const classes = ClassNames(
      'image',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-width-${widthClasses}`]: widthClasses },
      { [`is-ratio-${ratioClasses}`]: ratioClasses },
      className
    )

    return (
      <ImageWrapper
        className={classes}
      >
        <div className='image-ratio'>
          {
            (src || dataSrc) ?
              <img className={ClassNames(
                  'image-content',
                  { 'swiper-lazy': dataSrc } // lazyload detected class
                )}
                data-src={dataSrc}
                src={src}
                alt={alt}
              />
              :
              <img className={ClassNames(
                'image-placeholder',
                { 'swiper-lazy': dataSrcPlaceholder } // lazyload detected class
              )}
                data-src={dataSrcPlaceholder}
                src={srcPlaceholder}
                alt={altPlaceholder}
              />
          }
        </div>
        {children}
      </ImageWrapper>
    )
  }
}
