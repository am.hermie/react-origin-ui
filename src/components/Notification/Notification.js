import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  NotificationWrapper
} from './styled'

/**
 * Notification description:
 * - UI Template for use with react-toastify(https://github.com/fkhadra/react-toastify)
 */

export class Notification extends React.PureComponent {
  static defaultProps = {
    iconSize: 'sm',
    srcIconType: require('./images/icon-info-white.png'),
    srcIconClose: require('./images/icon-close-white.png')
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Status for display notification types
    */
    status: PropTypes.oneOf([
      'success',
      'error'
    ]),

    /**
    * Icon size
    */
    iconSize: PropTypes.oneOf([
      'sm',
      'md',
      'lg'
    ]),

    /**
    * Message
    */
    message: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Icon type is in front of message (Url image)
    */
    srcIconType: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string
    ]),

    /**
    * Icon close is in back of message (Url image)
    */
    srcIconClose: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      status,
      iconSize,
      message,
      srcIconType,
      srcIconClose
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const statusClasses = ClassNames(status)
    const iconSizeClasses = ClassNames(iconSize)
    const classes = ClassNames(
      'notification',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-status-${statusClasses}`]: statusClasses },
      { [`is-icon-size-${iconSizeClasses}`]: iconSizeClasses },
      className
    )

    return (
      <NotificationWrapper
        className={classes}
      >
        {
          srcIconType &&
            <img className='notification-icon' alt='Notification icon'
              src={srcIconType}
            />
        }
        <div className='notification-message'>
          {message}
        </div>
        <div className='notification-button-close'>
          <img className='notification-button-close-image' alt='Notification close button'
            src={srcIconClose}
          />
        </div>
        {children}
      </NotificationWrapper>
    )
  }
}
