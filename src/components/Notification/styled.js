import styled, {
  injectGlobal
} from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-Notification.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const NotificationWrapper = styled.div`
  /* Parent
  ------------------------------- */
  position: relative;
  display: flex;
  align-items: center;
  padding: 15px ${VARIABLES.NOTIFICATION.BUTTON_CLOSE.WIDTH} 15px 15px;
  margin: 15px;
  background-color: ${VARIABLES.COLORS.WHITE};
  border-radius: 4px;
  box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);

  /* Childrens
  ------------------------------- */
  .notification-icon {
    display: flex;
    justify-content: center;
    align-items: center;
    flex: none;
    margin-right: 15px;
  }

  .notification-icon-image {
    display: inline-block;
    height: auto;
    vertical-align: middle;
  }

  .notification-message {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_TN};
    color: ${VARIABLES.COLORS.TEXT_DETAIL};
  }

  .notification-button-close {
    position: absolute;
    z-index: ${VARIABLES.Z_INDEXS.LV_1};
    top: 0;
    right: 0;
    bottom: 0;
    display: flex;
    justify-content: center;
    align-items: center;
    width: ${VARIABLES.NOTIFICATION.BUTTON_CLOSE.WIDTH};
  }

  .notification-button-close-image {
    display: inline-block;
    width: 10px;
    height: auto;
    vertical-align: middle;
  }

  /* Modifiers
  ------------------------------- */
  /* Status */
  &.is-status-success,
  &.is-status-error {
    .notification-message {
      color: ${VARIABLES.COLORS.WHITE};
    }
  }

  &.is-status-success {
    background-color: ${VARIABLES.COLORS.SYSTEM_SUCCESS};
  }

  &.is-status-error {
    background-color: ${VARIABLES.COLORS.SYSTEM_ERROR};
  }

  /* Icon size */
  &.is-icon-size-sm {
    .notification-icon {
      width: ${VARIABLES.NOTIFICATION.ICON.WIDTHS.SM};
    }
  }

  &.is-icon-size-md {
    .notification-icon {
      width: ${VARIABLES.NOTIFICATION.ICON.WIDTHS.MD};
    }
  }

  &.is-icon-size-lg {
    .notification-icon {
      width: ${VARIABLES.NOTIFICATION.ICON.WIDTHS.LG};
    }
  }

  /* Media queries
  ------------------------------- */
`

// React Toastify - Overwrite style
injectGlobal`
  .react-toastify {
    /* Container */
    &.Toastify__toast-container {
      z-index: ${VARIABLES.Z_INDEXS.LV_6};
      width: ${VARIABLES.NOTIFICATION.WIDTHS.SM};
      padding: 0;
    }

    /* Position */
    &.Toastify__toast-container--top-left {
      top: 0;
      left: 0;
    }

    &.Toastify__toast-container--top-center {
      transform: translateX(-50%);
      top: 0;
      left: 50%;
      margin-left: 0;
    }

    &.Toastify__toast-container--top-right {
      top: 0;
      right: 0;
    }

    &.Toastify__toast-container--bottom-left {
      bottom: 0;
      left: 0;
    }

    &.Toastify__toast-container--bottom-center {
      transform: translateX(-50%);
      bottom: 0;
      left: 50%;
      margin-left: 0;
    }

    &.Toastify__toast-container--bottom-right {
      bottom: 0;
      right: 0;
    }

    /* List item */
    .Toastify__toast {
      min-height: 1px;
      padding: 0;
      margin-bottom: -15px;
      background-color: transparent;
      border-radius: 0;
      box-shadow: 0 0 0 transparent;
    }

    /* Close button */
    .Toastify__close-button {
      opacity: 1;
      position: absolute;
      z-index: ${VARIABLES.Z_INDEXS.LV_1};
      top: 15px;
      right: 15px;
      bottom: 15px;
      width: ${VARIABLES.NOTIFICATION.BUTTON_CLOSE.WIDTH};
      font-size: 0;
    }
  }

  @keyframes slideDownFadeIn {
    0% {
      opacity: 0;
      transform: translateY(-30px);
    }

    100% {
      opacity: 1;
      transform: translateY(0);
    }
  }

  .slideDownFadeIn {
    animation-name: slideDownFadeIn;
  }

  @keyframes slideDownFadeOut {
    0% {
      opacity: 1;
      transform: translateY(0);
    }

    100% {
      opacity: 0;
      transform: translateY(-30px);
    }
  }

  .slideDownFadeOut {
    animation-name: slideDownFadeOut;
  }
`