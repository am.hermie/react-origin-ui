import styled, {
  injectGlobal
} from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-Sortable.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const SortableWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  .sortable-item {
    /* UI */
    &.is-ui-parent {
      padding: 15px;
      margin-bottom: 15px;
      background-color: #CCCCCC;
      font-size: 48px;
    }

    &.is-ui-children {
      padding: 5px;
      margin-bottom: 5px;
      background-color: #EEEEEE;
      font-size: 24px;
    }

    &.is-ui-grid {
      padding: 15px;
      background-color: lightgrey;
    }
  }

  .sortable-handle {
    cursor: move;
    line-height: 1;

    /* UI */
    &.is-ui-parent {
      font-size: 48px;
      color: red;
    }

    &.is-ui-children {
      font-size: 32px;
      color: blue;
    }
  }

  /* Modifiers
  ------------------------------- */
  /* UI */
  &.is-ui-parent {}

  &.is-ui-children {}

  /* Media queries
  ------------------------------- */
`

// Fix sortable-item style of react-sortable-hoc when drag sortable-item, see scenario
  //
  // 1. drag sortable-item
  // 2. react-sortable-hoc will create sortable-item html copy at body
  // 3. component style doesn't working because a child element are outside a parent class
  // 4. use a child element global style for control this issue
  //
// !!! Please ensure, a child element global style doesn't overwrtire any style !!!

injectGlobal`
  /* Childrens
  ------------------------------- */
  .sortable-item {
    z-index: ${VARIABLES.Z_INDEXS.LV_1};

    /* UI */
    &.is-ui-parent {
      padding: 15px;
      margin-bottom: 15px;
      background-color: #CCCCCC;
      font-size: 48px;
    }

    &.is-ui-children {
      padding: 5px;
      margin-bottom: 5px;
      background-color: #EEEEEE;
      font-size: 24px;
    }

    &.is-ui-grid {
      padding: 15px;
      background-color: lightgrey;
    }
  }

  .sortable-handle {
    cursor: move;
    line-height: 1;

    /* UI */
    &.is-ui-parent {
      font-size: 48px;
      color: red;
    }

    &.is-ui-children {
      font-size: 32px;
      color: blue;
    }
  }

  /* Modifiers
  ------------------------------- */
  /* UI */
  &.is-ui-parent {}

  &.is-ui-children {}

  /* Media queries
  ------------------------------- */
`