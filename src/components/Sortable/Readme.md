Notice:

```jsx
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Sample react-anything-sortable usage, please see source file at:
  <br />
  - 'src/containers/Lab/Examples/Sortable/index.js'
  <br />
  - or 'npm start' and go to route '/lab/examples/sortable' for see live example
</div>
```
