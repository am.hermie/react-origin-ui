import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  SortableWrapper
} from './styled'

class SortableHandle extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render() {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'sortable-handle',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

class SortableItem extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render() {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'sortable-item',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        {children}
      </div>
    )
  }
}

/**
 * Sortable description:
 * - Wrapper/Container for add parent class and then overwrite default react-sortable-hoc style
 * - https://github.com/clauderic/react-sortable-hoc/
 */

export class Sortable extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  static Item = SortableItem
  static Handle = SortableHandle

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'sortable',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <SortableWrapper
        className={classes}
      >
        {children}
      </SortableWrapper>
    )
  }
}
