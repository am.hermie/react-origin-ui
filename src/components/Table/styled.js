import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  default as TYPOGRAPHYS
} from '../../themes/styles/bases/typographys'
import {
  default as MIXINS
} from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const TableWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  /* Scrollbar */
  .table-scroll {
    overflow: auto;

    &::-webkit-scrollbar {
      width: ${VARIABLES.SCROLLBAR.WIDTH};
      height: ${VARIABLES.SCROLLBAR.HEIGHT};
    }

    &::-webkit-scrollbar-button {
      width: 0;
      height: 0;
    }

    &::-webkit-scrollbar-thumb {
      background: ${VARIABLES.SCROLLBAR.THUMB.COLORS.DEFAULT};
      border: 0;
      border-radius: ${VARIABLES.SCROLLBAR.BORDER_RADIUS};

      &:hover {
        background: ${VARIABLES.SCROLLBAR.THUMB.COLORS.HOVER};
      }

      &:active {
        background: ${VARIABLES.SCROLLBAR.THUMB.COLORS.ACTIVE};
      }
    }

    &::-webkit-scrollbar-track {
      background: ${VARIABLES.SCROLLBAR.TRACK.COLORS.DEFAULT};
      border: 0;
      border-radius: ${VARIABLES.SCROLLBAR.BORDER_RADIUS};

      &:hover {
        background: ${VARIABLES.SCROLLBAR.TRACK.COLORS.HOVER};
      }

      &:active {
        background: ${VARIABLES.SCROLLBAR.TRACK.COLORS.ACTIVE};
      }
    }

    &::-webkit-scrollbar-corner {
      background: transparent;
    }
  }

  /* Row */
  .table-row {
    display: flex;

    &:last-child {
      .table-body-column {
        border-bottom-width: 0;
      }
    }

    /* Collapsible */
    &.is-collapsible {
      display: block;
    }
  }

  .table-row-container {
    display: flex;
  }

  /* Head container */
  .table-head {}

  /* Head/Body column */
  .table-head-column,
  .table-body-column {
    display: flex;
    flex: 1;
    width: 100%;
    padding: 10px;

    /* On click */
    &.is-onclick {
      cursor: pointer;
    }

    /* Width - fix column width (using with 'width' props) */
    &.is-width-narrow {
      flex: none;
    }

    /* Content align */
    &.is-content-align-center {
      justify-content: center;
    }

    &.is-content-align-right {
      justify-content: flex-end;
    }

    &.is-content-align-vertical-top {
      display: flex;

      > * {
        align-self: flex-start;
      }
    }

    &.is-content-align-vertical-center {
      display: flex;

      > * {
        align-self: center;
      }
    }

    &.is-content-align-vertical-bottom {
      display: flex;

      > * {
        align-self: flex-end;
      }
    }
  }

  /* Head column */
  .table-head-column {
    position: relative;
    border-bottom: 1px solid ${VARIABLES.COLORS.GRAY_1};

    &.is-sortable {
      .table-head-text {
        padding-right: 30px;
      }
    }
  }

  .table-head-column-sortable-arrow {
    pointer-events: none;
    position: absolute;
    z-index: ${VARIABLES.Z_INDEXS.LV_1};
    top: 0;
    right: 0;
    width: 30px;
    height: 100%;

    &:after {
      transform: translate(-50%, -50%);
      content: ' ';
      position: absolute;
      top: 50%;
      left: 50%;
      width: 14px;
      height: 9px;
      background-repeat: no-repeat;
      background-position: center;
      background-size: 14px 9px;
      /* Border arrow */
      /* width: 0;
      height: 0;
      margin-top: 2px;
      border: solid transparent;
      border-color: rgba(255, 255, 255, 0);
      border-top-color: ${VARIABLES.COLORS.TEXT_HEAD};
      border-width: 4px; */
    }

    &.is-ascending {
      &:after {
        background-image: url(${require('./images/icon-sortable-ascending.svg')});
      }
    }

    &.is-descending {
      &:after {
        background-image: url(${require('./images/icon-sortable-descending.svg')});
      }
    }
  }

  /* Body container */
  .table-body {}

  /* Body column */
  .table-body-column {
    position: relative;
    border-bottom: 1px solid ${VARIABLES.COLORS.GRAY_1};

    &.is-collapsible-arrow {
      .table-body-text {
        padding-right: 30px;
      }
    }
  }

  .table-body-column-collapsible-arrow {
    pointer-events: none;
    position: absolute;
    z-index: ${VARIABLES.Z_INDEXS.LV_1};
    top: 0;
    right: 0;
    width: 30px;
    height: 100%;

    &:after {
      transform: translate(-50%, -50%);
      content: ' ';
      position: absolute;
      top: 50%;
      left: 50%;
      width: 0;
      height: 0;
      border: solid transparent;
      border-color: rgba(255, 255, 255, 0);
      border-width: 4px;
    }

    &.is-open {
      &:after {
        margin-top: -2px;
        border-bottom-color: ${VARIABLES.COLORS.TEXT_HEAD};
      }
    }

    &.is-close {
      &:after {
        margin-top: 2px;
        border-top-color: ${VARIABLES.COLORS.TEXT_HEAD};
      }
    }
  }

  /* Text */
  .table-head-text {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_XS};
    color: ${VARIABLES.COLORS.TEXT_HEAD};
  }

  .table-body-text {
    ${TYPOGRAPHYS.FONT_STYLES.FIRST_REGULAR_XXS};
    color: ${VARIABLES.COLORS.TEXT_DETAIL};
    word-break: break-word;

    /* Ellipsis */
    &.is-ellipsis {
      ${MIXINS.ELLIPSIS({})};
    }
  }

  /* Group */
  .table-group {
    display: flex;
  }

  /* Collapsible */
  .table-collapsible {
    display: none;
    background-color: ${VARIABLES.COLORS.GRAY_1};

    &.is-open {
      display: block;
    }
  }

  /* Modifiers
  ------------------------------- */
  /* Media queries
  ------------------------------- */
  @media (min-width: ${VARIABLES.BREAKPOINTS.LAPTOP_MD_MIN}) {
    .table-head-column,
    .table-body-column {
      /* Width - allow narrow column (fix width) to fluid column (fluid width) at >= 1441px screen width */
      &.is-width-fluid {
        flex: 1;
        width: auto;
        max-width: 100% !important; /* use !important because overwrite inline style props */
      }
    }
  }
`
