Default:

```jsx
<div>Note: Can't display modal in styleguide because modal style is position 'fixed' cause to hide all viewport</div>
<br />
/* View & Copy code to your react project and change isOpen to 'true' state for display modal
==================================================

<Modal
  isOpen={false}
  onClickOverlay={() => {}}
>
  <Modal.Container>
    <Modal.Header>
      Modal_Heading
    </Modal.Header>
    <Modal.Body>
      Modal_Body
    </Modal.Body>
    <Modal.Footer>
      Modal_Footer
    </Modal.Footer>
  </Modal.Container>
</Modal>

==================================================
*/
```

Width:

```jsx
<div>Note: Can't display modal in styleguide because modal style is position 'fixed' cause to hide all viewport</div>
<br />
/* View & Copy code to your react project and change isOpen to 'true' state for display modal
==================================================

<Modal
  isOpen={false}
  onClickOverlay={() => {}}
>
  <Modal.Container width='md'>
    <Modal.Header>
      Modal_Heading
    </Modal.Header>
    <Modal.Body>
      Modal container width props:
      - 'sm' = 375px (default)
      - 'md' = 414px
      - 'lg' = 768px
    </Modal.Body>
    <Modal.Footer>
      Modal_Footer
    </Modal.Footer>
  </Modal.Container>
</Modal>

==================================================
*/
```

Customize Header and Footer:

```jsx
<div>Note: Can't display modal in styleguide because modal style is position 'fixed' cause to hide all viewport</div>
<br />
/* View & Copy code to your react project and change isOpen to 'true' state for display modal
==================================================

<Modal
  isOpen={false}
  onClickOverlay={() => {}}
>
  <Modal.Container>
    <Modal.Header
      title='Modal_Title'
      operations={
        <div>Opertions</div>
      }
    />
    <Modal.Body>
      Modal_Body
    </Modal.Body>
    <Modal.Footer
      contentLeft={
        <div>Left_Content</div>
      }
      contentRight={
        <div>Right_Content</div>
      }
    />
  </Modal.Container>
</Modal>

==================================================
*/
```

Footer content alignment:

```jsx
<div>Note: Can't display modal in styleguide because modal style is position 'fixed' cause to hide all viewport</div>
<br />
/* View & Copy code to your react project and change isOpen to 'true' state for display modal
==================================================

<Modal
  isOpen={false}
  onClickOverlay={() => {}}
>
  <Modal.Container>
    <Modal.Header>
      Modal_Heading
    </Modal.Header>
    <Modal.Body>
      Modal_Body
    </Modal.Body>
    <Modal.Footer contentAlign='spacebetween'>
      <div>Left_Content</div>
      <div>Right_Content</div>
    </Modal.Footer>
    <Modal.Footer contentAlign='right'>
      Modal_Footer
    </Modal.Footer>
    <Modal.Footer contentAlign='center'>
      Modal_Footer
    </Modal.Footer>
  </Modal.Container>
</Modal>

==================================================
*/
```

Body scroll (Enable body scroll when modal content is more than screen height):

```jsx
<div>Note: Can't display modal in styleguide because modal style is position 'fixed' cause to hide all viewport</div>
<br />
/* View & Copy code to your react project and change isOpen to 'true' state for display modal
==================================================

<Modal
  isScrollBody
  isOpen={false}
  onClickOverlay={() => {}}
>
  <Modal.Container width='md'>
    <Modal.Header>
      Modal_Heading
    </Modal.Header>
    <Modal.Body>
      <div style={{
        height: '2000px',
        backgroundColor: '#CCCCCC'
      }}>
        Modal_Body
      </div>
    </Modal.Body>
    <Modal.Footer>
      Modal_Footer
    </Modal.Footer>
  </Modal.Container>
</Modal>

==================================================
*/
```
