import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  ModalWrapper,
  ModalOverlayWrapper
} from './styled'

class ModalContainer extends React.PureComponent {
  static defaultProps = {
    width: 'sm'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Width
    */
    width: PropTypes.oneOf([
      'sm',
      'md',
      'lg'
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      width
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const widthClasses = ClassNames(width)
    const classes = ClassNames(
      'modal-container',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-width-${widthClasses}`]: widthClasses },
      className
    )

    return (
      <div className={classes}>
        {children}
      </div>
    )
  }
}

class ModalHeader extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Title
    */
    title: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Operations eg. close icon/button
    */
    operations: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      title,
      operations
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'modal-header',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-align-title-operations': operations },
      className
    )

    return (
      <div className={classes}>
        {
          title &&
            <div className='modal-header-title'>
              {title}
            </div>
        }
        {
          (!title && !operations) &&
            <React.Fragment>
              {children}
            </React.Fragment>
        }
        {
          operations &&
            <div className='modal-header-operations'>
              {operations}
            </div>
        }
      </div>
    )
  }
}

class ModalBody extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'modal-body',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <div className={classes}>
        {children}
      </div>
    )
  }
}

class ModalFooter extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Content alignment
    */
    contentAlign: PropTypes.oneOf([
      'spacebetween',
      'right',
      'center'
    ]),

    /**
    * Left content - additional elements or text
    */
    contentLeft: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Right content - additional elements or text
    */
    contentRight: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ])
  }

  render () {
    const {
      className,
      children,
      ui,
      contentAlign,
      contentLeft,
      contentRight
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const contentAlignClasses = ClassNames(contentAlign)
    const classes = ClassNames(
      'modal-footer',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-content-align-${contentAlignClasses}`]: contentAlignClasses },
      { [`is-content-align-spacebetween`]: contentLeft && contentRight },
      className
    )

    return (
      <div className={classes}>
        {
          contentLeft &&
            <div className='modal-footer-content'>
              {contentLeft}
            </div>
        }
        {
          (!contentLeft && !contentRight) &&
            <React.Fragment>
              {children}
            </React.Fragment>
        }
        {
          contentRight &&
            <div className='modal-footer-content'>
              {contentRight}
            </div>
        }
      </div>
    )
  }
}

/**
 * Modal description
 * - Floating Content block with overlay is on above a main layout
 */

export class Modal extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Display modal
    */
    isOpen: PropTypes.bool,

    /**
    * Enable body scroll when modal content is more than screen height
    */
    isScrollBody: PropTypes.bool,

    /**
    * On click event - Overlay
    */
    onClickOverlay: PropTypes.func
  }

  static Container = ModalContainer
  static Header = ModalHeader
  static Body = ModalBody
  static Footer = ModalFooter

  render () {
    const {
      className,
      children,
      ui,
      isOpen,
      isScrollBody,
      onClickOverlay
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'modal',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { 'is-open': isOpen },
      { 'is-scroll-body': isScrollBody },
      className
    )

    return (
      <React.Fragment>
        <ModalWrapper
          className={classes}
        >
          {children}
          <div className='modal-overlay-button'
            onClick={onClickOverlay}
          />
        </ModalWrapper>
        <ModalOverlayWrapper
          // Separate out from 'modal' becuase z-index bug in ios webview
          className={ClassNames(
            'modal-overlay',
            { 'is-open': isOpen }
          )}
        />
      </React.Fragment>
    )
  }
}
