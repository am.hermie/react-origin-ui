import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const ModalWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Transition duration must consistent with a children element for continuous animation */
  transition: ${VARIABLES.TRANSITIONS.DEFAULT};
  pointer-events: none;
  visibility: hidden;
  opacity: 0;
  overflow: auto;
  position: fixed;
  z-index: ${VARIABLES.Z_INDEXS.LV_4};
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;

  /* Childrens
  ------------------------------- */
  /* Container */
  .modal-container {
    transition: ${VARIABLES.TRANSITIONS.DEFAULT};
    transform: translateY(-30px);
    opacity: 0;
    position: relative;
    z-index: ${VARIABLES.Z_INDEXS.LV_3};
    width: 100%;
    margin: 30px 15px;
    background-color: ${VARIABLES.COLORS.WHITE};

    /* Width */
    &.is-width-sm {
      max-width: ${VARIABLES.MODAL.WIDTHS.SM};
    }

    &.is-width-md {
      max-width: ${VARIABLES.MODAL.WIDTHS.MD};
    }

    &.is-width-lg {
      max-width: ${VARIABLES.MODAL.WIDTHS.LG};
    }
  }

  /* Header */
  .modal-header {
    &.is-align-title-operations {
      display: flex;
      justify-content: space-between;
    }
  }

  .modal-header-title {
    flex: 1;
  }

  .modal-header-operations {
    flex: none;
    padding-left: 30px;
  }

  /* Body */
  .modal-body {}

  /* Footer */
  .modal-footer {
    /* Content align */
    &[class*='is-content-align-'] {
      display: flex;
      align-items: center;
    }

    &.is-content-align-spacebetween {
      justify-content: space-between;
    }

    &.is-content-align-right {
      justify-content: flex-end;
    }

    &.is-content-align-center {
      justify-content: center;
    }
  }

  /* Overlay button */
  /* Use instead 'modal-overlay' because 'modal-overlay' is under 'modal' */
  .modal-overlay-button {
    position: fixed;
    z-index: ${VARIABLES.Z_INDEXS.LV_2};
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  /* Modifiers
  ------------------------------- */
  /* Open */
  &.is-open {
    pointer-events: auto;
    visibility: visible;
    opacity: 1;

    .modal-container {
      transform: translateY(0);
      opacity: 1;
    }
  }

  /* Scroll */
  &.is-scroll-body {
    position: absolute;
    height: auto;
    min-height: 100%;
  }

  /* Media queries
  ------------------------------- */
`

// Overlay
// Separate out from 'modal' becuase z-index bug in ios webview
// ============================================================
export const ModalOverlayWrapper = styled.div`
  /* Parent
  ------------------------------- */
  transition: ${VARIABLES.TRANSITIONS.DEFAULT};
  pointer-events: none;
  opacity: 0;
  position: fixed;
  z-index: ${VARIABLES.Z_INDEXS.LV_2};
  top: 0;
  right: 0;
  width: 100%;
  height: 100%;
  background-color: ${VARIABLES.COLORS.OVERLAY_1};

  /* Childrens
  ------------------------------- */
  /* Modifiers
  ------------------------------- */
  &.is-open {
    pointer-events: auto;
    opacity: 1;
  }

  /* Media queries
  ------------------------------- */
`
