import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
import {
  ButtonWrapper
} from './styled'

/**
 * Button description:
 * - Variety of button
 */

export default class Button extends React.PureComponent {
  static defaultProps = {
    width: '125px',
    height: '32px',
    bgColor: '#e0e1e2',
    fontSize: '16px',
    fontColor: 'black',
    borderRadius: '6px',
    onClick: () => { },
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional text classes
    */
    textClass: PropTypes.string,

    /**
    * Add disabled style
    */
    disabled: PropTypes.bool,

    /**
    * Width:
    * element width with unit ex. 14px, 1em
    */
    width: PropTypes.string,

    /**
    * Height:
    * element height with unit ex. 14px, 1em
    */
    height: PropTypes.string,

    /**
    * Background color
    */
    bgColor: PropTypes.string,

    /**
    * Border radius
    */
    borderRadius: PropTypes.string,

    /**
    * Font Size:
    * text font size with unit ex. 14px, 1em
    */
    fontSize: PropTypes.string,

    /**
    * Font color
    */
    fontColor: PropTypes.string,

    /**
    * Icon image
    */
    icon: PropTypes.node,

    /**
    * Icon position
    */
    iconPosition: PropTypes.oneOf([
      'left',
      'right'
    ]),

    /**
    * On click event
    */
    onClick: PropTypes.func
  }

  render() {
    const {
      className,
      children,
      icon,
      iconPosition,
      onClick,
      textClass,
      width,
      height,
      bgColor,
      fontSize,
      fontColor,
      borderRadius,
      disabled
    } = this.props

    return (
      <ButtonWrapper
        className={ClassNames({
          disabled,
        }, 'button', className)}
        onClick={onClick}
        theme={{
          width,
          height,
          bgColor,
          fontSize,
          fontColor,
          borderRadius,
        }}
      >
        {
          icon && iconPosition === 'left' &&
          <div className='icon-left'>
            {icon}
          </div>
        }
        <div className={ClassNames(textClass,'text')}
        >
          {children}
        </div>
        {
          icon && iconPosition === 'right' &&
          <div className='icon-right'>
            {icon}
          </div>
        }
      </ButtonWrapper>
    )
  }
}
