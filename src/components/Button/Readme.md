Default:

```jsx
  <Button>
    Default
  </Button>
```

Width/Height/Color/Font size & color:

```jsx
  <Button width='50px' height='30px' bgColor='red' fontSize='18px' fontColor='white' textColor='green'>
    small
  </Button>
```

Custom text class:

```jsx
  <Button textClass='custom-text'>
    CustomText Class
  </Button>
```

Disabled:

```jsx
  <Button disabled>Default</Button>
```

Icon:

```jsx
  <Button iconPosition='left'
    icon={
      <img src={'./images/sample-icon.svg'} style={{ display: 'block', width: '14px', height: '14px' }} alt='Icon' />
    }
  >
    Extra
  </Button>
  <br />
  <Button iconPosition='right'
    icon={
      <img src={'./images/sample-icon.svg'} style={{ display: 'block', width: '14px', height: '14px' }} alt='Icon' />
    }
  >
    Extra
  </Button>
```
