import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
import {
  default as UTILITIES
} from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-sample.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const ButtonWrapper = styled.div`
  /* Parent
  ------------------------------- */
  transition: ${VARIABLES.TRANSITIONS.DEFAULT};
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  ${props => {
    const theme = props.theme
    return `
      width: ${theme.width};
      height: ${theme.height};
      background-color: ${theme.bgColor};
      border-radius: ${theme.borderRadius};
    `
  }}

  &.disabled{
    pointer-events: none;
    opacity: 0.5;
  }

  /* Childrens
  ------------------------------- */
  .text {
    padding: 0.5em 2em;
    white-space: nowrap;
    ${props => {
      const theme = props.theme
      return `
        color: ${theme.fontColor};
        font-size: ${theme.fontSize};
      `
    }}
  }

  .fa {
    &.icon-left {
      margin-right: 0.5em;
    }

    &.icon-right {
      margin-left: 0.5em;
    }
  }

  /* Modifiers
  ------------------------------- */

  /* Media queries
  ------------------------------- */
`
