Notice:

```jsx
<div style={{
  padding: '15px',
  marginTop: '15px',
  backgroundColor: '#EEEEEE'
}}>
  Sample react-quill usage, please see source file at:
  <br />
  - 'src/containers/Lab/Examples/TextEditor/index.js'
  <br />
  - or 'npm start' and go to route '/lab/examples/texteditor' for see live example
  <br />
  - html from react-quill: https://gist.github.com/Nattarat/c8329ff1ca82f12a111964d7e341fe19#file-react-quill-js
</div>
```
