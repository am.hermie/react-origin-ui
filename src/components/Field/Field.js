import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  FieldWrapper
} from './styled'

class FieldInput extends React.PureComponent {
  static defaultProps = {
    type: 'text',
    isText: true,
    buttonUploadFileName: 'Browse file'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Add <option> to <select>
    */
    children: PropTypes.node,

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([]),

    /**
    * Input type
    */
    type: PropTypes.oneOf([
      'text',
      'password',
      'file'
    ]),

    /**
    * Placeholder
    */
    placeholder: PropTypes.string,

    /**
    * ID
    */
    id: PropTypes.string,

    /**
    * Name
    */
    name: PropTypes.string,

    /**
    * Value
    */
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.object
    ]),

    /**
    * Event function
    */
    onChange: PropTypes.func,

    /**
    * Specifies the types of files that the server accepts (that can be submitted through a file upload)
    * ref: https://www.w3schools.com/tags/att_input_accept.asp
    */
    accept: PropTypes.string,

    /**
    * Label (field name at front of input)
    */
    label: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Label width
    */
    labelWidth: PropTypes.oneOf([
      'sm',
      'md',
      'lg'
    ]),

    /**
    * Data width (insert block)
    */
    dataWidth: PropTypes.oneOf([
      'sm',
      'md',
      'lg'
    ]),

    /**
    * Hint (description at back of input)
    */
    hint: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Message (description at bottom of input)
    */
    message: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Display textarea
    */
    isTextarea: PropTypes.bool,

    /**
    * Display select
    */
    isSelect: PropTypes.bool,

    /**
    * Display file upload
    */
    isFile: PropTypes.bool,

    /**
    * Display upload button after browse file
    */
    isBrowsed: PropTypes.bool,

    /**
    * Button upload file name eg. Browse file
    */
    buttonBrowseFileName: PropTypes.string,

    /**
    * Button upload file name eg. Upload
    */
    buttonUploadFileName: PropTypes.string,

    /**
    * Upload file name
    */
    fileName: PropTypes.string,

    /**
    * Display text info instead field
    */
    textInfo: PropTypes.string
  }

  render() {
    const {
      className,
      children,
      ui,
      type,
      placeholder,
      id,
      name,
      value,
      onChange,
      accept,
      label,
      labelWidth,
      dataWidth,
      hint,
      message,
      isTextarea,
      isSelect,
      isFile,
      isBrowsed,
      buttonBrowseFileName,
      buttonUploadFileName,
      fileName,
      textInfo
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const labelWidthClasses = ClassNames(labelWidth)
    const dataWidthClasses = ClassNames(dataWidth)
    const classes = ClassNames(
      'field-input',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-label-width-${labelWidthClasses}`]: labelWidthClasses },
      { [`is-data-width-${dataWidthClasses}`]: dataWidthClasses },
      className
    )

    return (
      <div
        className={classes}
      >
        <div className='field-input-container'>
          {
            label &&
              <label className='field-input-label'>
                {label}
              </label>
          }
          <div className='field-input-group'>
            {
              (!isTextarea && !isSelect && !isFile && !textInfo) &&
                <input
                  className='field-input-data'
                  type={type}
                  placeholder={placeholder}
                  id={id}
                  name={name}
                  value={value}
                  onChange={onChange}
                />
            }
            {
              isTextarea &&
                <textarea
                  className='field-input-data-area'
                  type={type}
                  placeholder={placeholder}
                  id={id}
                  name={name}
                  value={value}
                  onChange={onChange}
                />
            }
            {
              isSelect &&
                <React.Fragment>
                  <select
                    className='field-input-data-select'
                    id={id}
                    name={name}
                    value={value}
                    onChange={onChange}
                  >
                    {children}
                  </select>
                  <div className='field-input-data-select-arrow'></div>
                </React.Fragment>
            }
            {
              isFile &&
                <div className={ClassNames(
                  'field-input-data-file',
                  { 'is-browsed': isBrowsed }
                )}>
                  <div className='field-input-data-file-name'>
                    <span className='field-input-data-file-name-text'>
                      {fileName}
                    </span>
                  </div>
                  <div className='field-input-data-file-button'>
                    <input
                      type='file'
                      id={id}
                      name={name}
                      value={value}
                      onChange={onChange}
                      accept={accept}
                    />
                    <span className='field-input-data-file-button-text'>
                      {buttonBrowseFileName}
                    </span>
                  </div>
                  <div className='field-input-data-file-button is-upload'>
                    <span className='field-input-data-file-button-text'>
                      {buttonUploadFileName}
                    </span>
                  </div>
                </div>
            }
            {
              textInfo &&
                <div className='field-input-text-info'>
                  {textInfo}
                </div>
            }
            {
              message &&
                <div className='field-input-message'>
                  {message}
                </div>
            }
          </div>
          {
            hint &&
              <label className='field-input-hint'>
                {hint}
              </label>
          }
        </div>
      </div>
    )
  }
}

class FieldToggle extends React.PureComponent {
  static defaultProps = {
    type: 'checkbox'
  }

  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'radio',
      'switch'
    ]),

    /**
    * ID
    */
    id: PropTypes.string,

    /**
    * Name
    */
    name: PropTypes.string,

    /**
    * Value
    */
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.object
    ]),

    /**
    * Event function
    */
    onChange: PropTypes.func,

    /**
    * Label (field name at front of input)
    */
    label: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),
  }

  render() {
    const {
      className,
      children,
      ui,
      type,
      id,
      name,
      value,
      checked,
      onChange,
      label,
      labelSwitch
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'field-toggle',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-ui-radio`]: type === 'radio' },
      className
    )

    return (
      <div
        className={classes}
      >
        <input className='field-toggle-input'
          type={type}
          id={id}
          name={name}
          value={value}
          checked={checked}
          onChange={onChange}
        />
        <label className='field-toggle-label'
          htmlFor={id}
        >
          <span className='field-toggle-label-icon'>
            {
              labelSwitch &&
                <span className='field-toggle-label-text-switch'>
                  {labelSwitch}
                </span>
            }
          </span>
          <span className='field-toggle-label-text'>
            {label}
          </span>
        </label>
        {children}
      </div>
    )
  }
}

/**
 * Field description:
 * - Input data type
 */

export class Field extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    ui: PropTypes.oneOf([
      'disabled',
      'validation-error'
    ]),

    /**
    * Modifier name for show/hide element
    */
    display: PropTypes.oneOf([
      'message'
    ])
  }

  static Input = FieldInput
  static Toggle = FieldToggle

  render () {
    const {
      className,
      children,
      ui,
      display
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const displayClasses = ClassNames(display)
    const classes = ClassNames(
      'field',
      { [`is-ui-${uiClasses}`]: uiClasses },
      { [`is-display-${displayClasses}`]: displayClasses },
      className
    )

    return (
      <FieldWrapper
        className={classes}
      >
        {children}
      </FieldWrapper>
    )
  }
}
