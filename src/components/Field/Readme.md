Default:

```jsx
<Field>
  <Field.Input
    placeholder='Placeholder'
    id='default_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    label='Label'
    placeholder='Placeholder'
    id='default_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    type='password'
    label='Password'
    placeholder='Placeholder'
    id='default_3'
    name=''
    value='1234'
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    isTextarea
    label='Textarea'
    placeholder='Placeholder'
    id='default_4'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    isSelect
    label='Select'
    id='default_5'
    name=''
    value='2'
    onChange={() => {}}
  >
    <option value='1'>1</option>
    <option value='2'>2</option>
    <option value='3'>3</option>
  </Field.Input>
</Field>
<br />
<Field>
  <Field.Input
    label='Label'
    textInfo='Static_info_text'
  />
</Field>
```

Show hint & message:

```jsx
<Field display='message'>
  <Field.Input
    label='Label'
    placeholder='Placeholder'
    hint='Hint'
    message='Message'
    id='hint_message_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field display='message'>
  <Field.Input
    isTextarea
    label='Textarea'
    placeholder='Placeholder'
    hint='Hint'
    message='Message'
    id='hint_message_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field display='message'>
  <Field.Input
    isSelect
    label='Select'
    hint='Hint'
    message='Message'
    id='hint_message_3'
    name=''
    value='2'
    onChange={() => {}}
  >
    <option value='1'>1</option>
    <option value='2'>2</option>
    <option value='3'>3</option>
  </Field.Input>
</Field>
```

Label / Data width:

```jsx
<Field>
  <Field.Input
    labelWidth='sm'
    label='Label_Small'
    placeholder='Placeholder'
    id='label_width_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    labelWidth='md'
    label='Label_Medium'
    placeholder='Placeholder'
    id='label_width_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    labelWidth='lg'
    label='Label_Large'
    placeholder='Placeholder'
    id='label_width_3'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    dataWidth='sm'
    label='Label'
    placeholder='Placeholder'
    id='data_width_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    dataWidth='md'
    label='Label'
    placeholder='Placeholder'
    id='data_width_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    dataWidth='lg'
    label='Label'
    placeholder='Placeholder'
    id='data_width_3'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Input
    labelWidth='sm'
    label='Label_Small'
    textInfo='Static_info_text'
  />
</Field>
<br />
<Field>
  <Field.Input
    labelWidth='md'
    label='Label_Medium'
    textInfo='Static_info_text'
  />
</Field>
<br />
<Field>
  <Field.Input
    labelWidth='lg'
    label='Label_Large'
    textInfo='Static_info_text'
  />
</Field>
```

Checkbox / Radio:

```jsx
<Field>
  <Field.Toggle
    label='Label'
    checked={true}
    id='checkbox_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Toggle
    label='Label'
    checked={false}
    id='checkbox_2'
    name=''
    value=''
    onChange={() => {}}
  />
  <Field.Input
    placeholder='Placeholder'
    hint='Hint'
    id='checkbox_3'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Toggle
    type='radio'
    label='Label'
    checked={true}
    id='radio_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Toggle
    type='radio'
    label='Label'
    checked={false}
    id='radio_2'
    name=''
    value=''
    onChange={() => {}}
  />
  <Field.Input
    placeholder='Placeholder'
    hint='Hint'
    id='radio_3'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Toggle
    ui='switch'
    checked={false}
    id='switch_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field>
  <Field.Toggle
    ui='switch'
    labelSwitch='Enable'
    checked={true}
    id='switch_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
```

File:

```jsx
<div>Default</div>
<br />
<Field display='message'>
  <Field.Input
    isFile
    isBrowsed={false}
    accept='.jpg, .png, .gif, .svg'
    buttonBrowseFileName='Browse file'
    buttonUploadFileName='Upload'
    fileName=''
    label='Label'
    hint='Hint'
    message='Message'
    id='file_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<div>Browsed</div>
<br />
<Field display='message'>
  <Field.Input
    isFile
    isBrowsed={true}
    accept='image/*, video/*, audio/*'
    buttonBrowseFileName='Browse file'
    buttonUploadFileName='Upload'
    fileName='sample_file_name.jpg'
    label='Label'
    hint='Hint'
    message='Message'
    id='file_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
```

Validation:

```jsx
<Field ui='validation-error'>
  <Field.Input
    label='Label'
    placeholder='Placeholder'
    hint='Hint'
    message='Error_Message'
    id='validation_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='validation-error'>
  <Field.Input
    isTextarea
    label='Label'
    placeholder='Placeholder'
    hint='Hint'
    message='Error_Message'
    id='validation_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='validation-error'>
  <Field.Input
    isSelect
    label='Select'
    id='validation_3'
    hint='Hint'
    message='Error_Message'
    name=''
    value='2'
    onChange={() => {}}
  >
    <option value='1'>1</option>
    <option value='2'>2</option>
    <option value='3'>3</option>
  </Field.Input>
</Field>
<br />
<Field ui='validation-error'>
  <Field.Input
    isFile
    isBrowsed={true}
    buttonBrowseFileName='Browse file'
    buttonUploadFileName='Upload'
    fileName='sample_file_name.jpg'
    label='Label'
    hint='Hint'
    message='Error_Message'
    id='validation_4'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
```

UI:

```jsx
<div>Disabled</div>
<br />
<Field ui='disabled'>
  <Field.Input
    label='Label'
    placeholder='Placeholder'
    hint='Hint'
    id='ui_1'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='disabled'>
  <Field.Input
    isTextarea
    label='Label'
    placeholder='Placeholder'
    hint='Hint'
    id='ui_2'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='disabled'>
  <Field.Input
    isSelect
    label='Select'
    id='ui_3'
    hint='Hint'
    name=''
    value='2'
    onChange={() => {}}
  >
    <option value='1'>1</option>
    <option value='2'>2</option>
    <option value='3'>3</option>
  </Field.Input>
</Field>
<br />
<Field ui='disabled'>
  <Field.Toggle
    label='Label_Toggle_Sample'
    checked={false}
    id='ui_4'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='disabled'>
  <Field.Toggle
    label='Label'
    checked={false}
    id='ui_5'
    name=''
    value=''
    onChange={() => {}}
  />
  <Field.Input
    placeholder='Placeholder'
    hint='Hint'
    id='ui_6'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='disabled'>
  <Field.Toggle
    ui='switch'
    checked={false}
    id='switch_3'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
<br />
<Field ui='disabled'>
  <Field.Input
    isFile
    isBrowsed={false}
    buttonBrowseFileName='Browse file'
    buttonUploadFileName='Upload'
    label='Label'
    id='validation_4'
    name=''
    value=''
    onChange={() => {}}
  />
</Field>
```
