import React from 'react'
import ClassNames from 'classnames'
import PropTypes from 'prop-types'
import {
  AccordionControlWrapper
} from './styled'

/**
 * AccordionControl description:
 * - Wrapper/Container for add parent class and then overwrite default module style
 * - https://github.com/springload/react-accessible-accordion/
 */

export class AccordionControl extends React.PureComponent {
  static propTypes = { // TYPE > node, string, number, bool, array, object, symbol, func
    /**
    * Additional classes
    */
    className: PropTypes.string,

    /**
    * Additional elements or text
    */
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.string
    ]),

    /**
    * Modifier name for change default multiple UI (parent and children)
    */
    // ui: PropTypes.oneOf([])
  }

  render () {
    const {
      className,
      children,
      ui
    } = this.props

    // props for css classes
    const uiClasses = ClassNames(ui)
    const classes = ClassNames(
      'accordion-control',
      { [`is-ui-${uiClasses}`]: uiClasses },
      className
    )

    return (
      <AccordionControlWrapper
        className={classes}
      >
        {children}
      </AccordionControlWrapper>
    )
  }
}
