import styled from 'styled-components'
import {
  default as VARIABLES
} from '../../themes/styles/bases/variables'
// import {
//   default as TYPOGRAPHYS
// } from '../../themes/styles/bases/typographys'
// import {
//   default as MIXINS
// } from '../../themes/styles/helpers/mixins'
// import {
//   default as UTILITIES
// } from '../../themes/styles/helpers/utilities'
// import {
//   // Example using:
//   // background: url(${CONTENTS['image-AccordionControl.svg']});
//   CONTENTS,
//   ICONS,
//   LOGOS,
//   SHAREDS,
//   DOCUMENTS
// } from '../../themes'

// Wrapper
// ============================================================
export const AccordionControlWrapper = styled.div`
  /* Parent
  ------------------------------- */
  /* Childrens
  ------------------------------- */
  /* Container */
  .accordion {
    border: 1px solid ${VARIABLES.COLORS.GRAY_1};
  }

  /* List */
  .accordion__item {
    border-bottom: 1px solid ${VARIABLES.COLORS.GRAY_1};

    &:last-child {
      border-bottom-width: 0;
    }
  }

  /* Button */
  .accordion__title {
    cursor: pointer;
    position: relative;
    padding-right: 30px;

    &:focus {
      outline: none;
    }

    /* Arrow */
    &:before,
    &:after {
      content: ' ';
      position: absolute;
      z-index: ${VARIABLES.Z_INDEXS.LV_1};
      top: 50%;
      right: 11px;
      display: block;
      width: 8px;
      height: 2px;
      background-color: ${VARIABLES.COLORS.BLACK};
      border-radius: 2px;
    }

    &:before {
      transform: translate(-2px, -50%) rotate(45deg);
    }

    &:after {
      transform: translate(2px, -50%) rotate(-45deg);
    }

    /* Open content */
    &[aria-selected='true'] {
      &:before {
        transform: translate(-2px, -50%) rotate(-45deg);
      }

      &:after {
        transform: translate(2px, -50%) rotate(45deg);
      }
    }
  }

  /* Content */
  .accordion__body {
    display: block;
    background-color: ${VARIABLES.COLORS.GRAY_2};

    &.accordion__body--hidden {
      display: none;
    }
  }

  /* Modifiers
  ------------------------------- */
  /* Media queries
  ------------------------------- */
`
