import React from 'react'
import {
  Route,
  Switch
} from 'react-router-dom'
import {
  TransitionGroup,
  CSSTransition
} from 'react-transition-group'
import {
  ToastContainer,
  cssTransition
} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import {
  MainLayoutContainer,
  HomeContainer,
  DetailContainer,
  LabContainer,
  LabExampleFieldContainer,
  LabExampleMapDataContainer,
  LabExampleSortableContainer,
  LabExampleTextEditorContainer,
  LabExampleDateTimePickerContainer,
  LabExamplePaginationControlContainer,
  LabExampleAccordionControlContainer
} from 'containers'
import {
  context
} from 'context'
import {
  ROUTE_PATH
} from 'helpers'

export class RouteContainer extends React.Component {
  constructor(props) {
    super(props)
    context.setRedirect(this.props.history.push)
  }

  render() {
    const {
      location
    } = this.props

    const toastAnimation = cssTransition({
      enter: 'slideDownFadeIn',
      exit: 'slideDownFadeOut',
      duration: 300
    })

    return (
      <React.Fragment>
        <MainLayoutContainer>
          <TransitionGroup className='transition-group'>
            <CSSTransition classNames='page'
              key={location.key}
              timeout={{
                // Consistent with CSS transition duration in TransitionContainer style
                // File path: src/components/TransitionContainer/styled.js
                // 0.5s = 500
                enter: 500,
                exit: 500
              }}
            >
              <Switch location={location}>
                {/* Use props 'exact' for match single container(not share container) */}
                <Route exact path={ROUTE_PATH.HOME.LINK} component={HomeContainer} />
                <Route exact path={ROUTE_PATH.DETAIL.LINK} component={DetailContainer} />
                <Route exact path={ROUTE_PATH.LAB.LINK} component={LabContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.FIELD.LINK} component={LabExampleFieldContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.MAP_DATA.LINK} component={LabExampleMapDataContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.SORTABLE.LINK} component={LabExampleSortableContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.TEXT_EDITOR.LINK} component={LabExampleTextEditorContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.DATETIME_PICKER.LINK} component={LabExampleDateTimePickerContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.PAGINATION.LINK} component={LabExamplePaginationControlContainer} />
                <Route exact path={ROUTE_PATH.LAB.EXAMPLES.ACCORDION.LINK} component={LabExampleAccordionControlContainer} />
              </Switch>
            </CSSTransition>
          </TransitionGroup>
        </MainLayoutContainer>
        {/* React Toastify - global options */}
        <ToastContainer
          className='react-toastify'
          position='top-center'
          hideProgressBar
          closeOnClick={false}
          transition={toastAnimation}
        />
      </React.Fragment>
    )
  }
}
