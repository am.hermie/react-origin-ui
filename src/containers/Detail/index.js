import React from 'react'
import {
  TransitionContainer,
  Container
} from 'components'
import {
  ROUTE_PATH,
  redirect
} from 'helpers'

export class DetailContainer extends React.Component {
  render() {
    return (
      <TransitionContainer
        // motion='overlap-to'
      >
        <Container width='desktop'>
          <div style={{ width: '100%', height: '100vh', backgroundColor: 'lightskyblue' }}
            onClick={() => {
              redirect(ROUTE_PATH.HOME.LINK)
            }}
          >
            Detail
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
