import React from 'react'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import {
  Container,
  TransitionContainer,
  TextEditor
} from 'components'
import {
  REACT_QUILL_OPTION
} from 'helpers'

export class LabExampleTextEditorContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      textEditorContent: ''
    }
  }

  textEditor = {
    onTextEditorHandleChange: (value) => {
      this.setState({
        textEditorContent: value
      })
    },

    modules: {
      toolbar: REACT_QUILL_OPTION.TOOLBAR
    },

    formats: REACT_QUILL_OPTION.FORMATS
  }

  render() {
    const {
      textEditorContent
    } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Text editor example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>React Quill</h2>
            <TextEditor ui='react-quill'>
              <ReactQuill
                value={textEditorContent}
                modules={this.textEditor.modules}
                formats={this.textEditor.formats}
                onChange={this.textEditor.onTextEditorHandleChange}
              />
            </TextEditor>
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
