import React from 'react'
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody,
} from 'react-accessible-accordion'
import {
  Container,
  TransitionContainer,
  AccordionControl
} from 'components'

export class LabExampleAccordionControlContainer extends React.Component {
  // constructor() {
  //   super();
  //   this.state = {
  //   }
  // }

  // sample = {
  //   onChangeSample: () => {
  //     this.setState({
  //     })
  //   }
  // }

  render() {
    // const {
    // } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Accordion example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>React Accessible Accordion</h2>

            <AccordionControl>
              <Accordion
                accordion={true} // Enable accordion mode
                onChange={(itemUuid) => {
                  console.log(itemUuid);
                }}
              >
                <AccordionItem uuid='accordion-item-1'>
                  <AccordionItemTitle>Accordion_1</AccordionItemTitle>
                  <AccordionItemBody>Accordion_Content_1</AccordionItemBody>
                </AccordionItem>
                <AccordionItem uuid='accordion-item-2'>
                  <AccordionItemTitle>Accordion_2</AccordionItemTitle>
                  <AccordionItemBody>Accordion_Content_2</AccordionItemBody>
                </AccordionItem>
              </Accordion>
            </AccordionControl>
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
