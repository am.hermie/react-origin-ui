import React from 'react'
import {
  Container,
  TransitionContainer
} from 'components'

export class LabExampleMapDataContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      sampleData: [
        {
          title: 'Title 1',
          content: 'Content 1'
        },
        {
          title: 'Title 2',
          content: 'Content 2'
        },
        {
          title: 'Title 3',
          content: 'Content 3'
        }
      ]
    }
  }

  display = {
    displaySampleData: () => {
      const {
        sampleData
      } = this.state

      const listSample = sampleData.map((data, index) => {
        return (
            <div key={index} style={{
              padding: '15px',
              borderBottom: '1px solid #CCCCCC'
            }}>
              <h1>{data.title}</h1>
              <p>{data.content}</p>
            </div>
          )
        }
      )

      return (
        <div style={{
          backgroundColor: '#EEEEEE'
        }}>
          {listSample}
        </div>
      )
    }
  }

  render() {
    // const {
    // } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Map data to component example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>Default example</h2>
            {this.display.displaySampleData()}
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
