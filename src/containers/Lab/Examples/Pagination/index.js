import React from 'react'
import {
  Container,
  TransitionContainer,
  PaginationControl
} from 'components'

export class LabExamplePaginationControlContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      activePage: 3
    }
  }

  pagination = {
    onPaginationChange: (pageNumber) => {
      this.setState({
        activePage: pageNumber
      })
    }
  }

  render() {
    const {
      activePage
    } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Pagination example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>React JS Pagination</h2>
            <PaginationControl
              activePage={activePage}
              itemsCountPerPage={10}
              totalItemsCount={100}
              pageRangeDisplayed={5}
              onChange={this.pagination.onPaginationChange}
            />
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
