import React from 'react'
import {
  Container,
  TransitionContainer,
  Field
} from 'components'

export class LabExampleFieldContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      fieldSample_1: '',
      fieldSample_2: '',
      checkboxSample_1: false
    }
  }

  field = {
    onChangeInput: (ev) => {
      const target = ev.target
      const name = target.name
      const value = target.type === 'checkbox' ? target.checked : target.value
      this.setState({
        [name]: value
      })
    }
  }

  render() {
    const {
      fieldSample_1,
      fieldSample_2,
      checkboxSample_1
    } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Field with state and function example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>Default example</h2>
            <Field>
              <Field.Input
                label='Label_Sample'
                placeholder='Placeholder_Sample'
                hint='Hint_Sample'
                message='Message_Sample'
                id='fieldSample_1'
                name='fieldSample_1'
                value={fieldSample_1}
                onChange={this.field.onChangeInput}
              />
            </Field>
            <br />
            <Field>
              <Field.Toggle
                label='Label_Toggle_Sample'
                checked={checkboxSample_1}
                id='checkboxSample_1'
                name='checkboxSample_1'
                // value=''
                onChange={this.field.onChangeInput}
              />
              <Field.Input
                placeholder='Placeholder_Sample'
                hint='Hint_Sample'
                message='Message_Sample'
                id='fieldSample_2'
                name='fieldSample_2'
                value={fieldSample_2}
                onChange={this.field.onChangeInput}
              />
            </Field>
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
