import React from 'react'
import {
  sortableContainer,
  sortableElement,
  sortableHandle,
} from 'react-sortable-hoc'
import arrayMove from 'array-move'
import {
  Container,
  TransitionContainer,
  Sortable,
  Grid
} from 'components'

export class LabExampleSortableContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      gridItems: [
        'Grid 1',
        'Grid 2',
        'Grid 3'
      ],
      parentItems: [
        'Parent 1',
        'Parent 2',
        'Parent 3'
      ],
      childrenItems: [
        'Children 1',
        'Children 2',
        'Children 3'
      ]
    }
  }

  sortable = {
    onSortEndParent: ({ oldIndex, newIndex }) => {
      this.setState(({ parentItems }) => ({
        parentItems: arrayMove(parentItems, oldIndex, newIndex),
      }))
      console.log(this.state.parentItems, oldIndex, newIndex)
    },

    onSortEndChildren: ({ oldIndex, newIndex }) => {
      this.setState(({ childrenItems }) => ({
        childrenItems: arrayMove(childrenItems, oldIndex, newIndex),
      }))
      console.log(this.state.childrenItems, oldIndex, newIndex)
    },

    onSortEndGrid: ({ oldIndex, newIndex }) => {
      this.setState(({ gridItems }) => ({
        gridItems: arrayMove(gridItems, oldIndex, newIndex),
      }))
      console.log(this.state.gridItems, oldIndex, newIndex)
    }
  }

  render() {
    const {
      parentItems,
      childrenItems,
      gridItems
    } = this.state

    // Style
    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    // Grid
    const GridDragHandle = sortableHandle(() => (
      <Sortable.Handle ui='grid'>≡</Sortable.Handle>
    ))

    const GridSortableItem = sortableElement(({ value }) => (
      <Grid.Column isStackMobile col='4'>
        <Sortable.Item ui='grid'>
          <GridDragHandle />
          {value}
        </Sortable.Item>
      </Grid.Column>
    ))

    const GridSortableContainer = sortableContainer(({ children }) => {
      return (
        <Sortable ui='grid'>
          <Grid gutter='30' gutterVerticalMobile='30'>
            {children}
          </Grid>
        </Sortable>
      )
    })

    // Children
    const ChildrenDragHandle = sortableHandle(() => (
      <Sortable.Handle ui='children'>≡</Sortable.Handle>
    ))

    const ChildrenSortableItem = sortableElement(({ value }) => (
      <Sortable.Item ui='children'>
        <ChildrenDragHandle />
        {value}
      </Sortable.Item>
    ))

    const ChildrenSortableContainer = sortableContainer(({ children }) => {
      return (
        <Sortable ui='children'>
          {children}
        </Sortable>
      )
    })

    // Parent
    const ParentDragHandle = sortableHandle(() => (
      <Sortable.Handle ui='parent'>≡</Sortable.Handle>
    ))

    const ParentSortableItem = sortableElement(({ value }) => (
      <Sortable.Item ui='parent'>
        <ParentDragHandle />
        {value}
        <ChildrenSortableContainer
          useDragHandle
          onSortEnd={this.sortable.onSortEndChildren}
        >
          {
            childrenItems.map((value, index) => (
              <ChildrenSortableItem
                disabled={false} // Disabled dragging
                key={`children-item-${index}`}
                index={index}
                value={value}
              />
            ))
          }
        </ChildrenSortableContainer>
      </Sortable.Item>
    ))

    const ParentSortableContainer = sortableContainer(({ children }) => {
      return (
        <Sortable ui='parent'>
          {children}
        </Sortable>
      )
    })

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>React Sortable HOC example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>Sortable Grid</h2>
            <GridSortableContainer
              useDragHandle
              axis='xy'
              onSortEnd={this.sortable.onSortEndGrid}
            >
              {
                gridItems.map((value, index) => (
                  <GridSortableItem
                    disabled={false} // Disabled dragging
                    key={`grid-item-${index}`}
                    index={index}
                    value={value}
                  />
                ))
              }
            </GridSortableContainer>

            <h2 style={SUB_HEADING}>Sortable Row</h2>
            <ParentSortableContainer
              useDragHandle
              onSortEnd={this.sortable.onSortEndParent}
            >
              {
                parentItems.map((value, index) => (
                  <ParentSortableItem
                    disabled={false} // Disabled dragging
                    key={`parent-item-${index}`}
                    index={index}
                    value={value}
                  />
                ))
              }
            </ParentSortableContainer>
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
