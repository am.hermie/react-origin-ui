import React from 'react'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import {
  Container,
  TransitionContainer,
  DateTimePicker
} from 'components'

export class LabExampleDateTimePickerContainer extends React.Component {
  constructor() {
    super();
    this.state = {
      sampleDateTime: new Date()
    }
  }

  dateTimePicker = {
    onDateTimePickerHandleChange: (datetime) => {
      this.setState({
        sampleDateTime: datetime
      })
    }
  }

  render() {
    const {
      sampleDateTime
    } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Date picker example</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>React Date Picker - Date</h2>
            <DateTimePicker
              label='Label'
              hint='Hint'
            >
              <DatePicker
                isClearable
                placeholderText='Select date'
                dateFormat='yyyy/MM/dd' // MMMM d, yyyy
                selected={sampleDateTime}
                onChange={this.dateTimePicker.onDateTimePickerHandleChange}
              />
            </DateTimePicker>

            <h2 style={SUB_HEADING}>React Date Picker - Date time</h2>
            <DateTimePicker
              labelWidth='md'
              dataWidth='md'
              // Add mode='react-datepicker-datetime' for fix DatePicker(react-datepicker) container width when use 'showTimeSelect'
              mode='react-datepicker-datetime'
              label='Label'
              hint='Hint'
            >
              <DatePicker
                isClearable
                showTimeSelect
                placeholderText='Select date time'
                timeCaption='time'
                timeFormat='HH:mm'
                timeIntervals={30}
                dateFormat='yyyy/MM/dd, HH:mm' // MMMM d, yyyy hh:mm aa
                selected={sampleDateTime}
                onChange={this.dateTimePicker.onDateTimePickerHandleChange}
              />
            </DateTimePicker>

            <h2 style={SUB_HEADING}>React Date Picker - Disabled</h2>
            <DateTimePicker
              ui='disabled'
              label='Label'
              hint='Hint'
            >
              <DatePicker
                isClearable
                placeholderText='Select date'
                dateFormat='yyyy/MM/dd'
                selected={sampleDateTime}
                onChange={this.dateTimePicker.onDateTimePickerHandleChange}
              />
            </DateTimePicker>

            <h2 style={SUB_HEADING}>React Date Picker - Error</h2>
            <DateTimePicker
              ui='validation-error'
              label='Label'
              message='Message'
              hint='Hint'
            >
              <DatePicker
                isClearable
                placeholderText='Select date'
                dateFormat='yyyy/MM/dd'
                selected={sampleDateTime}
                onChange={this.dateTimePicker.onDateTimePickerHandleChange}
              />
            </DateTimePicker>

            <h2 style={SUB_HEADING}>React Date Picker - Error</h2>
            <DateTimePicker
              display='message'
              label='Label'
              message='Message'
              hint='Hint'
            >
              <DatePicker
                isClearable
                placeholderText='Select date'
                dateFormat='yyyy/MM/dd'
                selected={sampleDateTime}
                onChange={this.dateTimePicker.onDateTimePickerHandleChange}
              />
            </DateTimePicker>
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
