import React from 'react'
import {
  Container,
  TransitionContainer
} from 'components'
import {
  ROUTE_PATH,
  redirect
} from 'helpers'

export class LabContainer extends React.Component {
  // constructor() {
  //   super();
  //   this.state = {
  //   }
  // }

  // sample = {
  //   onChangeSample: () => {
  //     this.setState({
  //     })
  //   }
  // }

  render() {
    // const {
    // } = this.state

    const HEADING = {
      padding: '15px',
      backgroundColor: '#333333',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '24px',
      color: '#FFFFFF',
      textAlign: 'center'
    }

    const SUB_HEADING = {
      display: 'inline-block',
      padding: '5px 15px',
      marginTop: '45px',
      marginBottom: '30px',
      backgroundColor: '#666666',
      fontFamily: 'Helvetica, sans-serif',
      fontSize: '16px',
      color: '#FFFFFF',
    }

    const CONTENT = {
      padding: '0 30px 45px'
    }

    const TEXT_LINK = {
      cursor: 'pointer',
      display: 'inline-block',
      marginBottom: '10px',
      color: 'blue',
      textDecoration: 'underline'
    }

    return (
      <TransitionContainer>
        <Container width='desktop'>
          <h1 style={HEADING}>Lab is a page for test new component.</h1>
          <div style={CONTENT}>
            <h2 style={SUB_HEADING}>Examples:</h2>
            <ul>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.DATETIME_PICKER.LINK) }}>Datetime Picker</span></li>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.FIELD.LINK) }}>Field</span></li>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.MAP_DATA.LINK) }}>Map Data</span></li>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.PAGINATION.LINK) }}>Pagination</span></li>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.SORTABLE.LINK) }}>Sortable</span></li>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.TEXT_EDITOR.LINK) }}>Text Editor</span></li>
              <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.ACCORDION.LINK) }}>Accordion</span></li>
              {/* <li><span style={TEXT_LINK} onClick={() => { redirect(ROUTE_PATH.LAB.EXAMPLES.XXXXXXXXX.LINK) }}>XXXXXXXXX</span></li> */}
            </ul>
            <h2 style={SUB_HEADING}>Playground is here!!!</h2>
          </div>
        </Container>
      </TransitionContainer>
    )
  }
}
